﻿using System;
using SysMateriais.DI.Entidades;

namespace SysMateriais
{
    public partial class FrmConsultaVenda : DevExpress.XtraEditors.XtraForm
    {
        public FrmConsultaVenda()
        {
            InitializeComponent();
            ViewVendas.BestFitColumns();
        }

        private void FrmConsultaVenda_Load(object sender, EventArgs e)
        {
            RefreshData();
            ViewVendas.BestFitColumns();
        }

        private void RefreshData()
        {
            var refresh = new Caixa();

            gdVendas.DataSource = refresh.RetornaVendas();

            gdVendas.Refresh();

            ViewVendas.Columns[0].Caption = @"Código";
            ViewVendas.Columns[1].Caption = @"Cliente";
            ViewVendas.Columns[2].Caption = @"Produto";
        }
    }
}