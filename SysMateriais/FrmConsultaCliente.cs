﻿using System;
using System.Data;
using System.Windows.Forms;
using SysMateriais.DI.Entidades;

namespace SysMateriais
{
    public partial class FrmConsultaCliente : DevExpress.XtraEditors.XtraForm
    {
        FrmCaixa frmcad;
        public FrmConsultaCliente(FrmCaixa caixa)
        {
            this.frmcad = caixa;
            InitializeComponent();
        }

        private void FrmConsultaCliente_Load(object sender, EventArgs e)
        {
            RefreshData();
        }

        public void RefreshData()
        {
            var refresh = new Clientes();
            gridCli.DataSource = refresh.Data();
            gridCli.Refresh();
            viewCli.BestFitColumns();
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            var pesquisa = new Clientes();
            DataTable dtRetorno = pesquisa.RetornaPesquisa(txtPesquisa.Text);
            gridCli.DataSource = dtRetorno;
        }

        private void viewCli_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            object id = (viewCli.GetRowCellValue(e.RowHandle, "Id"));
            frmcad.txtCodCli.Text = id.ToString();
            object nome = viewCli.GetRowCellValue(viewCli.FocusedRowHandle, "Nome");
            frmcad.txtCliente.Text = nome.ToString();
            frmcad.txtCliente.Refresh();
            frmcad.txtCodCli.Refresh();
            frmcad.btnCodCli.Enabled = false;
            frmcad.lblInformacoes.Text = "Agora selecione o produto.";
            frmcad.btnCodPro.Enabled = true;
            this.Hide();
        }
    }
}