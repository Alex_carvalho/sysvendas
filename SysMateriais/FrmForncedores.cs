﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Xpo;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using SysMateriais.DI.Entidades;

namespace SysMateriais
{
    public partial class FrmForncedores : DevExpress.XtraEditors.XtraForm
    {
        public FrmForncedores()
        {
            InitializeComponent();
        }

        private string Modo = "";

        private void btnNovo_Click(object sender, EventArgs e)
        {
            gridControlFornecedor.Enabled = false;
            HabilitaCampos();
            LimpaCampos();
            btnCancelar.Enabled = true;
            btnEditar.Enabled = false;
            btnExcluir.Enabled = false;
            btnNovo.Enabled = false;
            btnSalvar.Enabled = true;
            txtNome.Focus();
            Modo = "Novo";
            lblInfo.Text = "Preencha todos os campos e clique em salvar para concluir.";
        }

        public void HabilitaCampos()
        {
            txtNome.Enabled = true;
            txtFantasia.Enabled = true;
            txtCnpj.Enabled = true;
            txtRamo.Enabled = true;
            txtEndereco.Enabled = true;
        }

        public void LimpaCampos()
        {
            txtNome.Text = "";
            txtFantasia.Text = "";
            txtCnpj.Text = "";
            txtRamo.Text = "";
            txtEndereco.Text = "";
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            gridControlFornecedor.Enabled = true;
            LimpaCampos();
            txtNome.Enabled = false;
            txtFantasia.Enabled = false;
            txtCnpj.Enabled = false;
            txtEndereco.Enabled = false;
            txtRamo.Enabled = false;
            DesabilitaCampos();
            lblInfo.Text = "";
        }

        private void DesabilitaCampos()
        {
            btnNovo.Enabled = true;
            btnCancelar.Enabled = false;
            btnEditar.Enabled = false;
            btnExcluir.Enabled = false;
            btnSalvar.Enabled = false;
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (Modo == "Novo")
            {
                if ((txtNome.Text == "") || (txtFantasia.Text == "") || (txtCnpj.Text == "") || (txtRamo.Text == "") || (txtEndereco.Text == ""))
                {
                    MessageBox.Show("É necessário preencher todos os campos para cadastrar.");
                }
                else
                {
                    var cadastra = new Fornecedor(txtNome.Text, txtFantasia.Text, txtEndereco.Text, txtCnpj.Text, txtRamo.Text);
                    bool retorno = cadastra.Cadastrar();
                    if (retorno == false)
                    {
                        MessageBox.Show("Ocorreu um erro ao cadastrar.");
                    }
                    else
                    {
                        gridControlFornecedor.Enabled = true;
                        MessageBox.Show("Cadastro efetuado com sucesso.");
                        LimpaCampos();
                        DesabilitaCampos();
                        txtNome.Enabled = false;
                        txtFantasia.Enabled = false;
                        txtCnpj.Enabled = false;
                        txtEndereco.Enabled = false;
                        txtRamo.Enabled = false;
                        btnEditar.Enabled = false;
                        btnExcluir.Enabled = false;
                        RefreshData();
                        lblInfo.Text = "";
                    }

                }
            }
            if (Modo == "Editar")
            {
                if ((txtNome.Text == "") || (txtFantasia.Text == "") || (txtCnpj.Text == "") || (txtEndereco.Text == "") || (txtRamo.Text == ""))
                {
                    MessageBox.Show("É necessário preencher todos os campos para alterar.");
                }
                else
                {
                    var altera = new Fornecedor(txtNome.Text, txtFantasia.Text, txtEndereco.Text, txtCnpj.Text, txtRamo.Text);
                    bool retorno = altera.AlteraFornecedor(Convert.ToInt32(txtCod.Text));
                    if (retorno == false)
                    {
                        MessageBox.Show("Ocorreu um erro ao alterar o fornecedor");
                    }
                    else
                    {
                        gridControlFornecedor.Enabled = true;
                        MessageBox.Show("Alteração efetuada com sucesso.");
                        LimpaCampos();
                        DesabilitaCampos();
                        txtNome.Enabled = false;
                        txtFantasia.Enabled = false;
                        txtCnpj.Enabled = false;
                        txtEndereco.Enabled = false;
                        txtRamo.Enabled = false;
                        btnEditar.Enabled = false;
                        btnExcluir.Enabled = false;
                        RefreshData();
                        lblInfo.Text = "";
                    }
                }
            }
            if (Modo == "Excluir")
            {
                var exclui = new Fornecedor();
                bool retorno = exclui.DeletaFornecedor(Convert.ToInt32(txtCod.Text));
                if (retorno == false)
                {
                    MessageBox.Show("Ocorreu um erro ao deletar o fornecedor.");
                }
                else
                {
                    gridControlFornecedor.Enabled = true;
                    MessageBox.Show("Exclusão do fornecedor efetuada com sucesso.");
                    LimpaCampos();
                    DesabilitaCampos();
                    txtNome.Enabled = false;
                    txtFantasia.Enabled = false;
                    txtCnpj.Enabled = false;
                    txtEndereco.Enabled = false;
                    txtRamo.Enabled = false;
                    btnEditar.Enabled = false;
                    btnExcluir.Enabled = false;
                    RefreshData();
                    lblInfo.Text = "";
                }
            }
        }

        private void RefreshData()
        {
            var refresh = new Fornecedor();
            gridControlFornecedor.DataSource = refresh.Data();
            gridControlFornecedor.Refresh();
            gridViewFornecedor.BestFitColumns();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            gridControlFornecedor.Enabled = false;
            Modo = "Editar";
            HabilitaCampos();
            btnNovo.Enabled = false;
            btnSalvar.Enabled = true;
            btnExcluir.Enabled = false;
            btnEditar.Enabled = false;
            btnCancelar.Enabled = true;
            lblInfo.Text = "Clique em salvar para efetuar a gravação no banco de dados\nou cancelar para abortar.";
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            gridControlFornecedor.Enabled = false;
            Modo = "Excluir";
            btnNovo.Enabled = false;
            btnSalvar.Enabled = true;
            btnExcluir.Enabled = false;
            btnEditar.Enabled = false;
            btnCancelar.Enabled = true;
            lblInfo.Text = "Clique em salvar para efetuar a exclusão do item no banco de dados\nou cancelar para abortar.";
        }

        private void FrmForncedores_Load(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            var pesquisa = new Fornecedor();
            DataTable dtRetorno = pesquisa.RetornaPesquisa(txtPesquisa.Text);
            gridControlFornecedor.DataSource = dtRetorno;
        }

        private void gridViewFornecedor_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            object id = (gridViewFornecedor.GetRowCellValue(e.RowHandle, "Id"));
            txtCod.Text = id.ToString();
            object nome = (gridViewFornecedor.GetRowCellValue(e.RowHandle, "Nome"));
            txtNome.Text = nome.ToString();
            object fantasia = (gridViewFornecedor.GetRowCellValue(e.RowHandle, "Fantasia"));
            txtFantasia.Text = fantasia.ToString();
            object cnpj = (gridViewFornecedor.GetRowCellValue(e.RowHandle, "CNPJ"));
            txtCnpj.Text = cnpj.ToString();
            object endereco = (gridViewFornecedor.GetRowCellValue(e.RowHandle, "Endereco"));
            txtEndereco.Text = endereco.ToString();
            object ramo = (gridViewFornecedor.GetRowCellValue(e.RowHandle, "Ramo"));
            txtRamo.Text = ramo.ToString();
        }
    }
}