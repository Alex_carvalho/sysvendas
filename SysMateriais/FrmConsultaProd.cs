﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Xpo;
using DevExpress.XtraEditors;
using SysMateriais.DI.Entidades;

namespace SysMateriais
{
    public partial class FrmConsultaProd : DevExpress.XtraEditors.XtraForm
    {
        FrmCaixa frmcad;

        public FrmConsultaProd(FrmCaixa frmcad)
        {
            this.frmcad = frmcad;
            InitializeComponent();
        }

        private void FrmConsultaProd_Load(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void RefreshData()
        {
            var refresh = new Produto();
            gridProd.DataSource = refresh.Data();
            gridProd.Refresh();
            viewProd.BestFitColumns();
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            var pesquisa = new Produto();
            DataTable dtRetorno = pesquisa.RetornaPesquisa(txtPesquisa.Text);
            gridProd.DataSource = dtRetorno;
        }

        private void viewProd_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            object codigo = (viewProd.GetRowCellValue(e.RowHandle, "Id"));
            frmcad.txtCodPro.Text = codigo.ToString();
            object nome = viewProd.GetRowCellValue(viewProd.FocusedRowHandle, "Nome");
            frmcad.txtProduto.Text = nome.ToString();
            object estoque = (viewProd.GetRowCellValue(viewProd.FocusedRowHandle, "Estoque"));
            frmcad.txtQtdEstoque.Text = estoque.ToString();
            object valorVenda = (viewProd.GetRowCellValue(viewProd.FocusedRowHandle, "ValorVenda"));
            frmcad.txtValor.Text = valorVenda.ToString();
            
            frmcad.txtQtdEstoque.Refresh();
            frmcad.txtCodPro.Refresh();
            frmcad.txtValor.Refresh();
            frmcad.txtProduto.Refresh();
            frmcad.txtQuantidade.Enabled = true;
            frmcad.txtQuantidade.Focus();
            frmcad.lblInformacoes.Text = "Digite a quantidade e clique em adicionar.";
            this.Hide();
        }
    }
}