﻿using System;
using System.Data;
using System.Globalization;
using System.Windows.Forms;
using SysMateriais.DI.Entidades;

namespace SysMateriais
{
    public partial class FrmCaixa : DevExpress.XtraEditors.XtraForm
    {
        public FrmCaixa()
        {
            InitializeComponent();
        }

        private void btnCodCli_Click(object sender, EventArgs e)
        {
            var frm = new FrmConsultaCliente(this);
            frm.Show();
        }

        private void btnCodPro_Click(object sender, EventArgs e)
        {
            var frm = new FrmConsultaProd(this);
            frm.Show();
        }

        private void FrmCaixa_Load(object sender, EventArgs e)
        {
            var caixa = new Caixa();
            int codigo = caixa.RetornaProximoId();
            txtCodVenda.Text = codigo == 0 ? "1" : codigo.ToString(CultureInfo.InvariantCulture);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var cadastra = new Caixa();
            double valorParcial = Convert.ToDouble(txtQuantidade.Text) * Convert.ToDouble(txtValor.Text);
            bool retorno = cadastra.RegistraVenda(Convert.ToInt32(txtCodCli.Text), Convert.ToInt32(txtCodPro.Text), 
                Convert.ToInt32(txtCodVenda.Text), valorParcial);
            if (retorno)
            {
                gridCli.DataSource = cadastra.RetornaDadosVenda(Convert.ToInt32(txtCodCli.Text), Convert.ToInt32(txtCodVenda.Text));
                
                ViewCli.BestFitColumns();

                ViewCli.Columns[0].Caption = @"Código";
                ViewCli.Columns[1].Caption = @"Cliente";
                ViewCli.Columns[2].Caption = @"Produto";
                ViewCli.Columns[3].Caption = @"ValorTotal";

                cadastra.AtualizaEstoque(Convert.ToInt32(txtCodPro.Text), Convert.ToInt32(txtQuantidade.Text));
                
                double valorTotal = 0;
                
                for (int i = 0; i < ViewCli.DataRowCount; i++)
                {
                    valorTotal = valorTotal + Convert.ToDouble(ViewCli.GetRowCellValue(i, "ValorTotal"));
                    txtValorTotal.Text = valorTotal + @",00";
                }

                txtQuantidade.Enabled = false;
                txtQuantidade.Text = "";
                btnAdd.Enabled = false;
                btnConcluir.Enabled = true;
                txtCodPro.Text = "";
                txtProduto.Text = "";
                txtValor.Text = "";
                txtQtdEstoque.Text = "";
                lblInformacoes.Text = "Selecione um novo produto ou clique em\nconcluir para fechar a venda.";
            }
            else
            {
                MessageBox.Show(@"Ocorreu um erro ao adicionar o produto.");
            }
        }

        private void btnConcluir_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(@"Deseja realmente fechar a venda? Lembrando que após fecha-la, não tem como reabri-la.", 
                                @"Aviso!", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {
                var caixa = new Caixa();
                bool retorno = caixa.FechaVenda(Convert.ToInt32(txtCodVenda.Text), txtValorTotal.Text);
                if (retorno)
                {
                    MessageBox.Show(@"Venda fechada com sucesso. Fechando caixa.");
                    Close();
                }
                else
                {
                    MessageBox.Show(@"Erro ao fechar venda, consulte o administrador.");
                }
            }
        }

        private void txtQuantidade_EditValueChanged(object sender, EventArgs e)
        {
            btnAdd.Enabled = txtQuantidade.Text != "";
        }
    }
}