﻿namespace SysMateriais
{
    partial class FrmConsultaFornec
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPesquisar = new DevExpress.XtraEditors.SimpleButton();
            this.lblPesquisa = new DevExpress.XtraEditors.LabelControl();
            this.txtPesquisa = new DevExpress.XtraEditors.TextEdit();
            this.lblInformacao = new System.Windows.Forms.Label();
            this.gridForn = new DevExpress.XtraGrid.GridControl();
            this.viewForne = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.txtPesquisa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridForn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewForne)).BeginInit();
            this.SuspendLayout();
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.Location = new System.Drawing.Point(7, 291);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(75, 23);
            this.btnPesquisar.TabIndex = 36;
            this.btnPesquisar.Text = "Pesquisar";
            this.btnPesquisar.Click += new System.EventHandler(this.btnPesquisar_Click);
            // 
            // lblPesquisa
            // 
            this.lblPesquisa.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPesquisa.Location = new System.Drawing.Point(6, 253);
            this.lblPesquisa.Name = "lblPesquisa";
            this.lblPesquisa.Size = new System.Drawing.Size(144, 16);
            this.lblPesquisa.TabIndex = 34;
            this.lblPesquisa.Text = "Pesquisa por Nome:";
            // 
            // txtPesquisa
            // 
            this.txtPesquisa.Location = new System.Drawing.Point(156, 252);
            this.txtPesquisa.Name = "txtPesquisa";
            this.txtPesquisa.Size = new System.Drawing.Size(293, 20);
            this.txtPesquisa.TabIndex = 35;
            // 
            // lblInformacao
            // 
            this.lblInformacao.AutoSize = true;
            this.lblInformacao.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblInformacao.Location = new System.Drawing.Point(114, 296);
            this.lblInformacao.Name = "lblInformacao";
            this.lblInformacao.Size = new System.Drawing.Size(362, 13);
            this.lblInformacao.TabIndex = 37;
            this.lblInformacao.Text = "Para selecionar um fornecedor clique duas vezes no fornecedor desejado.";
            // 
            // gridForn
            // 
            this.gridForn.Dock = System.Windows.Forms.DockStyle.Top;
            this.gridForn.Location = new System.Drawing.Point(0, 0);
            this.gridForn.MainView = this.viewForne;
            this.gridForn.Name = "gridForn";
            this.gridForn.Size = new System.Drawing.Size(528, 246);
            this.gridForn.TabIndex = 38;
            this.gridForn.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.viewForne});
            // 
            // viewForne
            // 
            this.viewForne.GridControl = this.gridForn;
            this.viewForne.Name = "viewForne";
            this.viewForne.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.viewForne_RowClick);
            // 
            // FrmConsultaFornec
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 331);
            this.Controls.Add(this.gridForn);
            this.Controls.Add(this.lblInformacao);
            this.Controls.Add(this.btnPesquisar);
            this.Controls.Add(this.lblPesquisa);
            this.Controls.Add(this.txtPesquisa);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.Name = "FrmConsultaFornec";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consulta de Fornecedores";
            this.Load += new System.EventHandler(this.FrmConsultaForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtPesquisa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridForn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewForne)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnPesquisar;
        private DevExpress.XtraEditors.LabelControl lblPesquisa;
        private DevExpress.XtraEditors.TextEdit txtPesquisa;
        private System.Windows.Forms.Label lblInformacao;
        private DevExpress.XtraGrid.GridControl gridForn;
        private DevExpress.XtraGrid.Views.Grid.GridView viewForne;
    }
}