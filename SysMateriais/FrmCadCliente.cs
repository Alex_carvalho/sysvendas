﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Xpo;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using SysMateriais.DI.Entidades;
using SysMateriais.DI.Factory;

namespace SysMateriais
{
    public partial class FrmCadCliente : DevExpress.XtraEditors.XtraForm
    {
        public FrmCadCliente()
        {
            InitializeComponent();
        }

        private string Modo = "";

        private void btnNovo_Click(object sender, EventArgs e)
        {
            gridControlCliente.Enabled = false;
            HabilitaCampos();
            LimpaCampos();
            btnCancelar.Enabled = true;
            btnEditar.Enabled = false;
            btnExcluir.Enabled = false;
            btnNovo.Enabled = false;
            btnSalvar.Enabled = true;
            txtNome.Focus();
            Modo = "Novo";
            lblInfo.Text = @"Preencha todos os campos e clique em salvar para concluir.";
        }

        public void HabilitaCampos()
        {
            txtNome.Enabled = true;
            txtEndereco.Enabled = true;
            txtCpf.Enabled = true;
            txtRg.Enabled = true;
            txtData.Enabled = true;
        }

        public void LimpaCampos()
        {
            txtEndereco.Text = "";
            txtNome.Text = "";
            txtCod.Text = "";
            txtCpf.Text = "";
            txtRg.Text = "";
            txtData.Text = "";
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            gridControlCliente.Enabled = true;
            LimpaCampos();
            txtNome.Enabled = false;
            txtEndereco.Enabled = false;
            txtCpf.Enabled = false;
            txtRg.Enabled = false;
            txtData.Enabled = false;
            DesabilitaCampos();
            lblInfo.Text = "";
        }

        private void DesabilitaCampos()
        {
            btnNovo.Enabled = true;
            btnCancelar.Enabled = false;
            btnEditar.Enabled = false;
            btnExcluir.Enabled = false;
            btnSalvar.Enabled = false;
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (Modo == "Novo")
            {
                if ((txtNome.Text == "") || (txtEndereco.Text == "") || (txtCpf.Text == "") || (txtData.Text == "") || (txtRg.Text == ""))
                {
                    MessageBox.Show(@"É necessário preencher todos os campos para cadastrar.");
                }
                else
                {
                    var cadastra = new Clientes(txtNome.Text, txtEndereco.Text, Convert.ToDateTime(txtData.Text), txtCpf.Text, txtRg.Text);
                    bool retorno = cadastra.Cadastrar();
                    if (retorno == false)
                    {
                        MessageBox.Show(@"Ocorreu um erro ao cadastrar.");
                    }
                    else
                    {
                        gridControlCliente.Enabled = true;
                        MessageBox.Show(@"Cadastro efetuado com sucesso.");
                        LimpaCampos();
                        DesabilitaCampos();
                        txtNome.Enabled = false;
                        txtEndereco.Enabled = false;
                        txtCpf.Enabled = false;
                        txtData.Enabled = false;
                        txtRg.Enabled = false;
                        btnEditar.Enabled = false;
                        btnExcluir.Enabled = false;
                        RefreshData();
                        lblInfo.Text = "";
                    }
                }
            }
            if (Modo == "Editar")
            {
                if ((txtNome.Text == "") || (txtEndereco.Text == "") || (txtCpf.Text == "") || (txtData.Text == "") || (txtRg.Text == ""))
                {
                    MessageBox.Show(@"É necessário preencher todos os campos para alterar.");
                }
                else
                {
                    var altera = new Clientes(txtNome.Text, txtEndereco.Text, Convert.ToDateTime(txtData.Text), txtCpf.Text, txtRg.Text);
                    bool retorno = altera.AlteraCli(Convert.ToInt32(txtCod.Text));
                    if (retorno == false)
                    {
                        MessageBox.Show("Ocorreu um erro ao alterar o cliente");
                    }
                    else
                    {
                        gridControlCliente.Enabled = true;
                        MessageBox.Show("Alteração efetuada com sucesso.");
                        LimpaCampos();
                        DesabilitaCampos();
                        txtNome.Enabled = false;
                        txtEndereco.Enabled = false;
                        txtCpf.Enabled = false;
                        txtData.Enabled = false;
                        txtRg.Enabled = false;
                        btnEditar.Enabled = false;
                        btnExcluir.Enabled = false;
                        RefreshData();
                        lblInfo.Text = "";
                    }
                }
            }
            if (Modo == "Excluir")
            {
                var exclui = new Clientes();
                bool retorno = exclui.DeletaCli(Convert.ToInt32(txtCod.Text));
                if (retorno == false)
                {
                    MessageBox.Show(@"Ocorreu um erro ao deletar o cliente");
                }
                else
                {
                    gridControlCliente.Enabled = true;
                    MessageBox.Show(@"Exclusão do cliente efetuada com sucesso.");
                    LimpaCampos();
                    DesabilitaCampos();
                    txtNome.Enabled = false;
                    txtEndereco.Enabled = false;
                    txtCpf.Enabled = false;
                    txtData.Enabled = false;
                    txtRg.Enabled = false;
                    btnEditar.Enabled = false;
                    btnExcluir.Enabled = false;
                    RefreshData();
                    lblInfo.Text = "";
                }
            }
        }

        private void RefreshData()
        {
            var refresh = new Clientes();
            gridControlCliente.DataSource = refresh.Data();
            gridControlCliente.Refresh();
            gridViewCliente.BestFitColumns();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            gridControlCliente.Enabled = false;
            Modo = "Editar";
            HabilitaCampos();
            btnNovo.Enabled = false;
            btnSalvar.Enabled = true;
            btnExcluir.Enabled = false;
            btnEditar.Enabled = false;
            btnCancelar.Enabled = true;
            lblInfo.Text = "Clique em salvar para efetuar a gravação no banco de dados\nou cancelar para abortar.";
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            gridControlCliente.Enabled = false;
            Modo = "Excluir";
            btnNovo.Enabled = false;
            btnSalvar.Enabled = true;
            btnExcluir.Enabled = false;
            btnEditar.Enabled = false;
            btnCancelar.Enabled = true;
            lblInfo.Text = "Clique em salvar para efetuar a exclusão do item no banco de dados\nou cancelar para abortar.";
        }

        private void FrmCadCliente_Load(object sender, EventArgs e)
        {
            RefreshData();
        }
        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            var pesquisa = new Clientes();
            DataTable dtRetorno = pesquisa.RetornaPesquisa(txtPesquisa.Text);
            gridControlCliente.DataSource = null;
            gridControlCliente.DataSource = dtRetorno;
            gridViewCliente.BestFitColumns();
        }

        private void gridViewCliente_RowClick(object sender, RowClickEventArgs e)
        {
            object id = (gridViewCliente.GetRowCellValue(e.RowHandle, "Id"));
            txtCod.Text = id.ToString();
            object nome = gridViewCliente.GetRowCellValue(gridViewCliente.FocusedRowHandle, "Nome");
            txtNome.Text = nome.ToString();
            object endereco = gridViewCliente.GetRowCellValue(gridViewCliente.FocusedRowHandle, "Endereco");
            txtEndereco.Text = endereco.ToString();
            object cpf = gridViewCliente.GetRowCellValue(gridViewCliente.FocusedRowHandle, "CPF");
            txtCpf.Text = cpf.ToString();
            object rg = gridViewCliente.GetRowCellValue(gridViewCliente.FocusedRowHandle, "RG");
            txtRg.Text = rg.ToString();
            object data = gridViewCliente.GetRowCellValue(gridViewCliente.FocusedRowHandle, "DataNascimento");
            txtData.Text = data.ToString();
            gridViewCliente.BestFitColumns();
        }
    }
}