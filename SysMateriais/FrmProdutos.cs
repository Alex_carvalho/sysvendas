﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Xpo;
using DevExpress.XtraEditors;
using SysMateriais.DI.Entidades;

namespace SysMateriais
{
    public partial class FrmProdutos : DevExpress.XtraEditors.XtraForm
    {
        public FrmProdutos()
        {
            InitializeComponent();
        }

        private string Modo = "";

        private void btnNovo_Click(object sender, EventArgs e)
        {
            gridControlProd.Enabled = false;
            HabilitaCampos();
            LimpaCampos();
            btnCancelar.Enabled = true;
            btnEditar.Enabled = false;
            btnExcluir.Enabled = false;
            btnNovo.Enabled = false;
            btnSalvar.Enabled = true;
            txtNome.Focus();
            Modo = "Novo";
            lblInfo.Text = "Preencha todos os campos e clique em salvar para concluir.";
        }

        public void HabilitaCampos()
        {
            txtNome.Enabled = true;
            txtEstoque.Enabled = true;
            txtVenda.Enabled = true;
            txtCompra.Enabled = true;
            btnPesquisar.Enabled = true;
        }

        public void LimpaCampos()
        {
            txtNome.Text = "";
            txtFornecedor.Text = "";
            txtEstoque.Text = "";
            txtCompra.Text = "";
            txtVenda.Text = "";
            txtCod.Text = "";
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (Modo == "Novo")
            {
                if ((txtNome.Text == "") || (txtFornecedor.Text == "") || (txtEstoque.Text == "") || (txtVenda.Text == "") || (txtCompra.Text == ""))
                {
                    MessageBox.Show("É necessário preencher todos os campos para cadastrar.");
                }
                else
                {
                    var cadastra = new Produto(txtNome.Text, txtFornecedor.Text, txtEstoque.Text, txtCompra.Text, txtVenda.Text);
                    bool retorno = cadastra.Cadastrar();
                    if (retorno == false)
                    {
                        MessageBox.Show("Ocorreu um erro ao cadastrar.");
                    }
                    else
                    {
                        gridControlProd.Enabled = true;
                        MessageBox.Show("Cadastro efetuado com sucesso.");
                        LimpaCampos();
                        DesabilitaCampos();
                        txtNome.Enabled = false;
                        txtFornecedor.Enabled = false;
                        txtEstoque.Enabled = false;
                        txtCompra.Enabled = false;
                        txtVenda.Enabled = false;
                        btnEditar.Enabled = false;
                        btnExcluir.Enabled = false;
                        RefreshData();
                        lblInfo.Text = "";
                    }

                }
            }
            if (Modo == "Editar")
            {
                if ((txtNome.Text == "") || (txtFornecedor.Text == "") || (txtEstoque.Text == "") || (txtCompra.Text == "") || (txtVenda.Text == ""))
                {
                    MessageBox.Show("É necessário preencher todos os campos para alterar.");
                }
                else
                {
                    var altera = new Produto(txtNome.Text, txtFornecedor.Text, txtEstoque.Text, txtCompra.Text, txtVenda.Text);
                    bool retorno = altera.AlteraProduto(Convert.ToInt32(txtCod.Text));
                    if (retorno == false)
                    {
                        MessageBox.Show("Ocorreu um erro ao alterar o produto.");
                    }
                    else
                    {
                        gridControlProd.Enabled = true;
                        MessageBox.Show("Alteração efetuada com sucesso.");
                        LimpaCampos();
                        DesabilitaCampos();
                        txtNome.Enabled = false;
                        txtFornecedor.Enabled = false;
                        txtEstoque.Enabled = false;
                        txtCompra.Enabled = false;
                        txtVenda.Enabled = false;
                        btnEditar.Enabled = false;
                        btnExcluir.Enabled = false;
                        RefreshData();
                        lblInfo.Text = "";
                    }
                }
            }
            if (Modo == "Excluir")
            {
                var exclui = new Produto();
                bool retorno = exclui.DeletaProduto(Convert.ToInt32(txtCod.Text));
                if (retorno == false)
                {
                    MessageBox.Show("Ocorreu um erro ao deletar o produto.");
                }
                else
                {
                    gridControlProd.Enabled = true;
                    MessageBox.Show("Exclusão do produto efetuada com sucesso.");
                    LimpaCampos();
                    DesabilitaCampos();
                    txtNome.Enabled = false;
                    txtFornecedor.Enabled = false;
                    txtEstoque.Enabled = false;
                    txtCompra.Enabled = false;
                    txtVenda.Enabled = false;
                    btnEditar.Enabled = false;
                    btnExcluir.Enabled = false;
                    RefreshData();
                    lblInfo.Text = "";
                }
            }
        }

        private void RefreshData()
        {
            var refresh = new Produto();
            gridControlProd.DataSource = refresh.Data();
            gridControlProd.Refresh();
            gridViewProd.BestFitColumns();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            gridControlProd.Enabled = true;
            LimpaCampos();
            txtNome.Enabled = false;
            txtFornecedor.Enabled = false;
            txtEstoque.Enabled = false;
            txtCompra.Enabled = false;
            txtVenda.Enabled = false;
            DesabilitaCampos();
            lblInfo.Text = "";
        }

        private void DesabilitaCampos()
        {
            btnNovo.Enabled = true;
            btnCancelar.Enabled = false;
            btnEditar.Enabled = false;
            btnExcluir.Enabled = false;
            btnSalvar.Enabled = false;
        }

        private void FrmProdutos_Load(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            gridControlProd.Enabled = false;
            Modo = "Editar";
            HabilitaCampos();
            btnNovo.Enabled = false;
            btnSalvar.Enabled = true;
            btnExcluir.Enabled = false;
            btnEditar.Enabled = false;
            btnCancelar.Enabled = true;
            lblInfo.Text = "Clique em salvar para efetuar a gravação no banco de dados\nou cancelar para abortar.";
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            gridControlProd.Enabled = false;
            Modo = "Excluir";
            btnNovo.Enabled = false;
            btnSalvar.Enabled = true;
            btnExcluir.Enabled = false;
            btnEditar.Enabled = false;
            btnCancelar.Enabled = true;
            lblInfo.Text = "Clique em salvar para efetuar a exclusão do item no banco de dados\nou cancelar para abortar.";
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            var pesquisa = new Produto();
            DataTable dtRetorno = pesquisa.RetornaPesquisa(txtPesquisa.Text);
            gridControlProd.DataSource = dtRetorno;
        }

        private void btnProcurar_Click(object sender, EventArgs e)
        {
            var consultaforn = new FrmConsultaFornec(this);
            consultaforn.Show();
        }

        private void gridViewProd_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            object id = (gridViewProd.GetRowCellValue(e.RowHandle, "Id"));
            txtCod.Text = id.ToString();
            object nome = (gridViewProd.GetRowCellValue(e.RowHandle, "Nome"));
            txtNome.Text = nome.ToString();
            object fornec = (gridViewProd.GetRowCellValue(e.RowHandle, "Fornecedor"));
            txtFornecedor.Text = fornec.ToString();
            object qtde = (gridViewProd.GetRowCellValue(e.RowHandle, "Estoque"));
            txtEstoque.Text = qtde.ToString();
            object compra = (gridViewProd.GetRowCellValue(e.RowHandle, "ValorCompra"));
            txtCompra.Text = compra.ToString();
            object venda = (gridViewProd.GetRowCellValue(e.RowHandle, "ValorVenda"));
            txtVenda.Text = venda.ToString();
        }
    }
}