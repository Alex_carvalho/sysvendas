﻿namespace SysMateriais
{
    partial class FrmCaixa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCodCli = new DevExpress.XtraEditors.LabelControl();
            this.lblCodFor = new DevExpress.XtraEditors.LabelControl();
            this.txtCliente = new DevExpress.XtraEditors.TextEdit();
            this.lblCliente = new DevExpress.XtraEditors.LabelControl();
            this.txtProduto = new DevExpress.XtraEditors.TextEdit();
            this.lblProduto = new DevExpress.XtraEditors.LabelControl();
            this.txtCodVenda = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lblValor = new DevExpress.XtraEditors.LabelControl();
            this.txtValor = new DevExpress.XtraEditors.TextEdit();
            this.lblEstoque = new DevExpress.XtraEditors.LabelControl();
            this.txtQtdEstoque = new DevExpress.XtraEditors.TextEdit();
            this.lblDispo = new DevExpress.XtraEditors.LabelControl();
            this.txtQuantidade = new DevExpress.XtraEditors.TextEdit();
            this.gridCli = new DevExpress.XtraGrid.GridControl();
            this.ViewCli = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.btnConcluir = new DevExpress.XtraEditors.SimpleButton();
            this.txtValorTotal = new DevExpress.XtraEditors.TextEdit();
            this.lblTotal = new DevExpress.XtraEditors.LabelControl();
            this.txtCodCli = new DevExpress.XtraEditors.TextEdit();
            this.txtCodPro = new DevExpress.XtraEditors.TextEdit();
            this.btnCodCli = new DevExpress.XtraEditors.SimpleButton();
            this.btnCodPro = new DevExpress.XtraEditors.SimpleButton();
            this.lblInformacoes = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.txtCliente.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProduto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodVenda.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQtdEstoque.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantidade.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCli)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViewCli)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValorTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodCli.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodPro.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCodCli
            // 
            this.lblCodCli.Location = new System.Drawing.Point(12, 33);
            this.lblCodCli.Name = "lblCodCli";
            this.lblCodCli.Size = new System.Drawing.Size(37, 13);
            this.lblCodCli.TabIndex = 1;
            this.lblCodCli.Text = "Código:";
            // 
            // lblCodFor
            // 
            this.lblCodFor.Location = new System.Drawing.Point(12, 89);
            this.lblCodFor.Name = "lblCodFor";
            this.lblCodFor.Size = new System.Drawing.Size(37, 13);
            this.lblCodFor.TabIndex = 2;
            this.lblCodFor.Text = "Código:";
            // 
            // txtCliente
            // 
            this.txtCliente.Location = new System.Drawing.Point(127, 52);
            this.txtCliente.Name = "txtCliente";
            this.txtCliente.Size = new System.Drawing.Size(516, 20);
            this.txtCliente.TabIndex = 6;
            // 
            // lblCliente
            // 
            this.lblCliente.Location = new System.Drawing.Point(127, 33);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(37, 13);
            this.lblCliente.TabIndex = 7;
            this.lblCliente.Text = "Cliente:";
            // 
            // txtProduto
            // 
            this.txtProduto.Location = new System.Drawing.Point(127, 108);
            this.txtProduto.Name = "txtProduto";
            this.txtProduto.Size = new System.Drawing.Size(234, 20);
            this.txtProduto.TabIndex = 8;
            // 
            // lblProduto
            // 
            this.lblProduto.Location = new System.Drawing.Point(127, 89);
            this.lblProduto.Name = "lblProduto";
            this.lblProduto.Size = new System.Drawing.Size(42, 13);
            this.lblProduto.TabIndex = 9;
            this.lblProduto.Text = "Produto:";
            // 
            // txtCodVenda
            // 
            this.txtCodVenda.Location = new System.Drawing.Point(575, 12);
            this.txtCodVenda.Name = "txtCodVenda";
            this.txtCodVenda.Size = new System.Drawing.Size(68, 20);
            this.txtCodVenda.TabIndex = 10;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(484, 15);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(85, 13);
            this.labelControl1.TabIndex = 11;
            this.labelControl1.Text = "Código da Venda:";
            // 
            // lblValor
            // 
            this.lblValor.Location = new System.Drawing.Point(367, 89);
            this.lblValor.Name = "lblValor";
            this.lblValor.Size = new System.Drawing.Size(28, 13);
            this.lblValor.TabIndex = 13;
            this.lblValor.Text = "Valor:";
            // 
            // txtValor
            // 
            this.txtValor.Location = new System.Drawing.Point(367, 108);
            this.txtValor.Name = "txtValor";
            this.txtValor.Size = new System.Drawing.Size(88, 20);
            this.txtValor.TabIndex = 12;
            // 
            // lblEstoque
            // 
            this.lblEstoque.Location = new System.Drawing.Point(461, 89);
            this.lblEstoque.Name = "lblEstoque";
            this.lblEstoque.Size = new System.Drawing.Size(71, 13);
            this.lblEstoque.TabIndex = 15;
            this.lblEstoque.Text = "Qtde/Estoque:";
            // 
            // txtQtdEstoque
            // 
            this.txtQtdEstoque.Location = new System.Drawing.Point(461, 108);
            this.txtQtdEstoque.Name = "txtQtdEstoque";
            this.txtQtdEstoque.Size = new System.Drawing.Size(88, 20);
            this.txtQtdEstoque.TabIndex = 14;
            // 
            // lblDispo
            // 
            this.lblDispo.Location = new System.Drawing.Point(555, 89);
            this.lblDispo.Name = "lblDispo";
            this.lblDispo.Size = new System.Drawing.Size(52, 13);
            this.lblDispo.TabIndex = 17;
            this.lblDispo.Text = "Disponivel:";
            // 
            // txtQuantidade
            // 
            this.txtQuantidade.Location = new System.Drawing.Point(555, 108);
            this.txtQuantidade.Name = "txtQuantidade";
            this.txtQuantidade.Size = new System.Drawing.Size(88, 20);
            this.txtQuantidade.TabIndex = 16;
            this.txtQuantidade.EditValueChanged += new System.EventHandler(this.txtQuantidade_EditValueChanged);
            // 
            // gridCli
            // 
            this.gridCli.Location = new System.Drawing.Point(12, 134);
            this.gridCli.MainView = this.ViewCli;
            this.gridCli.Name = "gridCli";
            this.gridCli.Size = new System.Drawing.Size(631, 260);
            this.gridCli.TabIndex = 18;
            this.gridCli.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.ViewCli});
            // 
            // ViewCli
            // 
            this.ViewCli.GridControl = this.gridCli;
            this.ViewCli.Name = "ViewCli";
            this.ViewCli.OptionsSelection.MultiSelect = true;
            this.ViewCli.OptionsView.ShowPreview = true;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(12, 400);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 19;
            this.btnAdd.Text = "Adicionar";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnConcluir
            // 
            this.btnConcluir.Location = new System.Drawing.Point(93, 400);
            this.btnConcluir.Name = "btnConcluir";
            this.btnConcluir.Size = new System.Drawing.Size(75, 23);
            this.btnConcluir.TabIndex = 20;
            this.btnConcluir.Text = "Concluir";
            this.btnConcluir.Click += new System.EventHandler(this.btnConcluir_Click);
            // 
            // txtValorTotal
            // 
            this.txtValorTotal.Location = new System.Drawing.Point(555, 401);
            this.txtValorTotal.Name = "txtValorTotal";
            this.txtValorTotal.Size = new System.Drawing.Size(88, 20);
            this.txtValorTotal.TabIndex = 21;
            // 
            // lblTotal
            // 
            this.lblTotal.Appearance.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(512, 405);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(37, 13);
            this.lblTotal.TabIndex = 22;
            this.lblTotal.Text = "Total:";
            // 
            // txtCodCli
            // 
            this.txtCodCli.Location = new System.Drawing.Point(12, 52);
            this.txtCodCli.Name = "txtCodCli";
            this.txtCodCli.Size = new System.Drawing.Size(49, 20);
            this.txtCodCli.TabIndex = 23;
            // 
            // txtCodPro
            // 
            this.txtCodPro.Location = new System.Drawing.Point(12, 108);
            this.txtCodPro.Name = "txtCodPro";
            this.txtCodPro.Size = new System.Drawing.Size(49, 20);
            this.txtCodPro.TabIndex = 24;
            // 
            // btnCodCli
            // 
            this.btnCodCli.Location = new System.Drawing.Point(67, 52);
            this.btnCodCli.Name = "btnCodCli";
            this.btnCodCli.Size = new System.Drawing.Size(36, 20);
            this.btnCodCli.TabIndex = 25;
            this.btnCodCli.Text = "...";
            this.btnCodCli.Click += new System.EventHandler(this.btnCodCli_Click);
            // 
            // btnCodPro
            // 
            this.btnCodPro.Location = new System.Drawing.Point(67, 108);
            this.btnCodPro.Name = "btnCodPro";
            this.btnCodPro.Size = new System.Drawing.Size(36, 20);
            this.btnCodPro.TabIndex = 26;
            this.btnCodPro.Text = "...";
            this.btnCodPro.Click += new System.EventHandler(this.btnCodPro_Click);
            // 
            // lblInformacoes
            // 
            this.lblInformacoes.AutoSize = true;
            this.lblInformacoes.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInformacoes.ForeColor = System.Drawing.Color.Blue;
            this.lblInformacoes.Location = new System.Drawing.Point(12, 9);
            this.lblInformacoes.Name = "lblInformacoes";
            this.lblInformacoes.Size = new System.Drawing.Size(309, 13);
            this.lblInformacoes.TabIndex = 27;
            this.lblInformacoes.Text = "Para começar é necessário informar o cliente.";
            // 
            // FrmCaixa
            // 
            this.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(662, 438);
            this.Controls.Add(this.lblInformacoes);
            this.Controls.Add(this.btnCodPro);
            this.Controls.Add(this.btnCodCli);
            this.Controls.Add(this.txtCodPro);
            this.Controls.Add(this.txtCodCli);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.txtValorTotal);
            this.Controls.Add(this.btnConcluir);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.gridCli);
            this.Controls.Add(this.lblDispo);
            this.Controls.Add(this.txtQuantidade);
            this.Controls.Add(this.lblEstoque);
            this.Controls.Add(this.txtQtdEstoque);
            this.Controls.Add(this.lblValor);
            this.Controls.Add(this.txtValor);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.txtCodVenda);
            this.Controls.Add(this.lblProduto);
            this.Controls.Add(this.txtProduto);
            this.Controls.Add(this.lblCliente);
            this.Controls.Add(this.txtCliente);
            this.Controls.Add(this.lblCodFor);
            this.Controls.Add(this.lblCodCli);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmCaixa";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Caixa";
            this.Load += new System.EventHandler(this.FrmCaixa_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtCliente.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProduto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodVenda.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQtdEstoque.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantidade.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCli)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViewCli)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValorTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodCli.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodPro.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblCodCli;
        private DevExpress.XtraEditors.LabelControl lblCodFor;
        public DevExpress.XtraEditors.TextEdit txtCliente;
        private DevExpress.XtraEditors.LabelControl lblCliente;
        public DevExpress.XtraEditors.TextEdit txtProduto;
        private DevExpress.XtraEditors.LabelControl lblProduto;
        private DevExpress.XtraEditors.TextEdit txtCodVenda;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl lblValor;
        public DevExpress.XtraEditors.TextEdit txtValor;
        private DevExpress.XtraEditors.LabelControl lblEstoque;
        public DevExpress.XtraEditors.TextEdit txtQtdEstoque;
        private DevExpress.XtraEditors.LabelControl lblDispo;
        public DevExpress.XtraEditors.TextEdit txtQuantidade;
        private DevExpress.XtraGrid.GridControl gridCli;
        private DevExpress.XtraGrid.Views.Grid.GridView ViewCli;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraEditors.SimpleButton btnConcluir;
        private DevExpress.XtraEditors.TextEdit txtValorTotal;
        private DevExpress.XtraEditors.LabelControl lblTotal;
        public DevExpress.XtraEditors.TextEdit txtCodCli;
        public DevExpress.XtraEditors.TextEdit txtCodPro;
        public DevExpress.XtraEditors.SimpleButton btnCodCli;
        public DevExpress.XtraEditors.SimpleButton btnCodPro;
        public System.Windows.Forms.Label lblInformacoes;
    }
}