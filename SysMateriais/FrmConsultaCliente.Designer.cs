﻿namespace SysMateriais
{
    partial class FrmConsultaCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPesquisar = new DevExpress.XtraEditors.SimpleButton();
            this.lblPesquisa = new DevExpress.XtraEditors.LabelControl();
            this.txtPesquisa = new DevExpress.XtraEditors.TextEdit();
            this.gridCli = new DevExpress.XtraGrid.GridControl();
            this.viewCli = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.lblInformacao = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.txtPesquisa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCli)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewCli)).BeginInit();
            this.SuspendLayout();
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.Location = new System.Drawing.Point(6, 292);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(75, 23);
            this.btnPesquisar.TabIndex = 41;
            this.btnPesquisar.Text = "Pesquisar";
            this.btnPesquisar.Click += new System.EventHandler(this.btnPesquisar_Click);
            // 
            // lblPesquisa
            // 
            this.lblPesquisa.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPesquisa.Location = new System.Drawing.Point(6, 257);
            this.lblPesquisa.Name = "lblPesquisa";
            this.lblPesquisa.Size = new System.Drawing.Size(144, 16);
            this.lblPesquisa.TabIndex = 39;
            this.lblPesquisa.Text = "Pesquisa por Nome:";
            // 
            // txtPesquisa
            // 
            this.txtPesquisa.Location = new System.Drawing.Point(156, 256);
            this.txtPesquisa.Name = "txtPesquisa";
            this.txtPesquisa.Size = new System.Drawing.Size(293, 20);
            this.txtPesquisa.TabIndex = 40;
            // 
            // gridCli
            // 
            this.gridCli.Dock = System.Windows.Forms.DockStyle.Top;
            this.gridCli.Location = new System.Drawing.Point(0, 0);
            this.gridCli.MainView = this.viewCli;
            this.gridCli.Name = "gridCli";
            this.gridCli.Size = new System.Drawing.Size(531, 250);
            this.gridCli.TabIndex = 43;
            this.gridCli.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.viewCli});
            // 
            // viewCli
            // 
            this.viewCli.GridControl = this.gridCli;
            this.viewCli.Name = "viewCli";
            this.viewCli.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.viewCli_RowClick);
            // 
            // lblInformacao
            // 
            this.lblInformacao.AutoSize = true;
            this.lblInformacao.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblInformacao.Location = new System.Drawing.Point(153, 297);
            this.lblInformacao.Name = "lblInformacao";
            this.lblInformacao.Size = new System.Drawing.Size(318, 13);
            this.lblInformacao.TabIndex = 44;
            this.lblInformacao.Text = "Para selecionar um cliente clique duas vezes no cliente desejado.";
            // 
            // FrmConsultaCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(531, 322);
            this.Controls.Add(this.lblInformacao);
            this.Controls.Add(this.gridCli);
            this.Controls.Add(this.btnPesquisar);
            this.Controls.Add(this.lblPesquisa);
            this.Controls.Add(this.txtPesquisa);
            this.Name = "FrmConsultaCliente";
            this.Text = "Consulta de Clientes";
            this.Load += new System.EventHandler(this.FrmConsultaCliente_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtPesquisa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCli)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewCli)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnPesquisar;
        private DevExpress.XtraEditors.LabelControl lblPesquisa;
        private DevExpress.XtraEditors.TextEdit txtPesquisa;
        private DevExpress.XtraGrid.GridControl gridCli;
        private DevExpress.XtraGrid.Views.Grid.GridView viewCli;
        private System.Windows.Forms.Label lblInformacao;
    }
}