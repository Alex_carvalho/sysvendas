﻿using System;
using System.Windows.Forms;
using DevExpress.Xpo;
using SysMateriais.DI.Entidades;
using SysMateriais.DI.Factory;

namespace SysMateriais
{
    public partial class FrmLogin : DevExpress.XtraEditors.XtraForm
    {
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            bool retorno;
            var login = new UsuariosFactory();
            retorno = login.VerificaLogin(txtUsuario.Text, txtSenha.Text);
            if (retorno == true)
            {
                var principal = new FormPrincipal();
                principal.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Nome de usuário ou senha inválidos.");
            }
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            Application.Exit();}
         
        private void FrmLogin_Load(object sender, EventArgs e)
        {
            var usuario = new Usuarios();
            if (usuario.RetornaSeExiste() == false)
            {
                MessageBox.Show("No momento não existe nenhum registro de usuário no banco de dados.\n É necessário cadastrar um para entrar no sistema, clique em ok para abrir o formulário para cadastra-lo.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                var frmcad = new FrmCadUsuarios();
                frmcad.ShowDialog();
            }
        }
    }
}