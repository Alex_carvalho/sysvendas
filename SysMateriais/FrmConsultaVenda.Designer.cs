﻿namespace SysMateriais
{
    partial class FrmConsultaVenda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gdVendas = new DevExpress.XtraGrid.GridControl();
            this.ViewVendas = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.gdVendas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViewVendas)).BeginInit();
            this.SuspendLayout();
            // 
            // gdVendas
            // 
            this.gdVendas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gdVendas.Location = new System.Drawing.Point(0, 0);
            this.gdVendas.MainView = this.ViewVendas;
            this.gdVendas.Name = "gdVendas";
            this.gdVendas.Size = new System.Drawing.Size(716, 348);
            this.gdVendas.TabIndex = 0;
            this.gdVendas.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.ViewVendas});
            // 
            // ViewVendas
            // 
            this.ViewVendas.GridControl = this.gdVendas;
            this.ViewVendas.Name = "ViewVendas";
            // 
            // FrmConsultaVenda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(716, 348);
            this.Controls.Add(this.gdVendas);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmConsultaVenda";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consulta de Vendas";
            this.Load += new System.EventHandler(this.FrmConsultaVenda_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gdVendas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ViewVendas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gdVendas;
        private DevExpress.XtraGrid.Views.Grid.GridView ViewVendas;
    }
}