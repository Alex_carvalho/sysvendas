﻿namespace SysMateriais
{
    partial class FrmForncedores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSalvar = new DevExpress.XtraEditors.SimpleButton();
            this.txtEndereco = new DevExpress.XtraEditors.TextEdit();
            this.txtCnpj = new DevExpress.XtraEditors.TextEdit();
            this.txtFantasia = new DevExpress.XtraEditors.TextEdit();
            this.txtNome = new DevExpress.XtraEditors.TextEdit();
            this.txtCod = new DevExpress.XtraEditors.TextEdit();
            this.lblRamo = new DevExpress.XtraEditors.LabelControl();
            this.btnPesquisar = new DevExpress.XtraEditors.SimpleButton();
            this.btnExcluir = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditar = new DevExpress.XtraEditors.SimpleButton();
            this.lblEndereco = new DevExpress.XtraEditors.LabelControl();
            this.lblCnpj = new DevExpress.XtraEditors.LabelControl();
            this.lblFantasia = new DevExpress.XtraEditors.LabelControl();
            this.btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.lblNome = new DevExpress.XtraEditors.LabelControl();
            this.lblPesquisa = new DevExpress.XtraEditors.LabelControl();
            this.lblCodigo = new DevExpress.XtraEditors.LabelControl();
            this.lblInfo = new DevExpress.XtraEditors.LabelControl();
            this.groupControlInfo = new DevExpress.XtraEditors.GroupControl();
            this.gridViewFornecedor = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridControlFornecedor = new DevExpress.XtraGrid.GridControl();
            this.txtPesquisa = new DevExpress.XtraEditors.TextEdit();
            this.btnNovo = new DevExpress.XtraEditors.SimpleButton();
            this.groupDados = new DevExpress.XtraEditors.GroupControl();
            this.txtRamo = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndereco.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCnpj.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFantasia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNome.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlInfo)).BeginInit();
            this.groupControlInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewFornecedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlFornecedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPesquisa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupDados)).BeginInit();
            this.groupDados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRamo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(104, 378);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(75, 23);
            this.btnSalvar.TabIndex = 27;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // txtEndereco
            // 
            this.txtEndereco.EditValue = "";
            this.txtEndereco.Location = new System.Drawing.Point(111, 186);
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(328, 20);
            this.txtEndereco.TabIndex = 10;
            // 
            // txtCnpj
            // 
            this.txtCnpj.Location = new System.Drawing.Point(111, 140);
            this.txtCnpj.Name = "txtCnpj";
            this.txtCnpj.Size = new System.Drawing.Size(218, 20);
            this.txtCnpj.TabIndex = 9;
            // 
            // txtFantasia
            // 
            this.txtFantasia.Location = new System.Drawing.Point(111, 98);
            this.txtFantasia.Name = "txtFantasia";
            this.txtFantasia.Size = new System.Drawing.Size(328, 20);
            this.txtFantasia.TabIndex = 8;
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(111, 58);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(328, 20);
            this.txtNome.TabIndex = 7;
            // 
            // txtCod
            // 
            this.txtCod.Location = new System.Drawing.Point(111, 24);
            this.txtCod.Name = "txtCod";
            this.txtCod.Size = new System.Drawing.Size(65, 20);
            this.txtCod.TabIndex = 1;
            // 
            // lblRamo
            // 
            this.lblRamo.Location = new System.Drawing.Point(5, 234);
            this.lblRamo.Name = "lblRamo";
            this.lblRamo.Size = new System.Drawing.Size(31, 13);
            this.lblRamo.TabIndex = 6;
            this.lblRamo.Text = "Ramo:";
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.Location = new System.Drawing.Point(850, 9);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(75, 23);
            this.btnPesquisar.TabIndex = 33;
            this.btnPesquisar.Text = "Pesquisar";
            this.btnPesquisar.Click += new System.EventHandler(this.btnPesquisar_Click);
            // 
            // btnExcluir
            // 
            this.btnExcluir.Location = new System.Drawing.Point(347, 378);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnExcluir.TabIndex = 30;
            this.btnExcluir.Text = "Excluir";
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.Location = new System.Drawing.Point(266, 378);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(75, 23);
            this.btnEditar.TabIndex = 29;
            this.btnEditar.Text = "Editar";
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // lblEndereco
            // 
            this.lblEndereco.Location = new System.Drawing.Point(5, 192);
            this.lblEndereco.Name = "lblEndereco";
            this.lblEndereco.Size = new System.Drawing.Size(49, 13);
            this.lblEndereco.TabIndex = 5;
            this.lblEndereco.Text = "Endereço:";
            // 
            // lblCnpj
            // 
            this.lblCnpj.Location = new System.Drawing.Point(5, 150);
            this.lblCnpj.Name = "lblCnpj";
            this.lblCnpj.Size = new System.Drawing.Size(29, 13);
            this.lblCnpj.TabIndex = 4;
            this.lblCnpj.Text = "CNPJ:";
            // 
            // lblFantasia
            // 
            this.lblFantasia.Location = new System.Drawing.Point(5, 108);
            this.lblFantasia.Name = "lblFantasia";
            this.lblFantasia.Size = new System.Drawing.Size(45, 13);
            this.lblFantasia.TabIndex = 3;
            this.lblFantasia.Text = "Fantasia:";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(185, 378);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 28;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // lblNome
            // 
            this.lblNome.Location = new System.Drawing.Point(5, 66);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(31, 13);
            this.lblNome.TabIndex = 2;
            this.lblNome.Text = "Nome:";
            // 
            // lblPesquisa
            // 
            this.lblPesquisa.Location = new System.Drawing.Point(460, 15);
            this.lblPesquisa.Name = "lblPesquisa";
            this.lblPesquisa.Size = new System.Drawing.Size(95, 13);
            this.lblPesquisa.TabIndex = 31;
            this.lblPesquisa.Text = "Pesquisa por Nome:";
            // 
            // lblCodigo
            // 
            this.lblCodigo.Location = new System.Drawing.Point(5, 24);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(37, 13);
            this.lblCodigo.TabIndex = 1;
            this.lblCodigo.Text = "Código:";
            // 
            // lblInfo
            // 
            this.lblInfo.Location = new System.Drawing.Point(5, 37);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(0, 13);
            this.lblInfo.TabIndex = 0;
            // 
            // groupControlInfo
            // 
            this.groupControlInfo.Controls.Add(this.lblInfo);
            this.groupControlInfo.Location = new System.Drawing.Point(460, 291);
            this.groupControlInfo.Name = "groupControlInfo";
            this.groupControlInfo.Size = new System.Drawing.Size(465, 110);
            this.groupControlInfo.TabIndex = 25;
            this.groupControlInfo.Text = "Informações:";
            // 
            // gridViewFornecedor
            // 
            this.gridViewFornecedor.GridControl = this.gridControlFornecedor;
            this.gridViewFornecedor.Name = "gridViewFornecedor";
            this.gridViewFornecedor.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridViewFornecedor_RowClick);
            // 
            // gridControlFornecedor
            // 
            this.gridControlFornecedor.Location = new System.Drawing.Point(460, 47);
            this.gridControlFornecedor.MainView = this.gridViewFornecedor;
            this.gridControlFornecedor.Name = "gridControlFornecedor";
            this.gridControlFornecedor.Size = new System.Drawing.Size(465, 234);
            this.gridControlFornecedor.TabIndex = 24;
            this.gridControlFornecedor.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewFornecedor});
            // 
            // txtPesquisa
            // 
            this.txtPesquisa.Location = new System.Drawing.Point(561, 12);
            this.txtPesquisa.Name = "txtPesquisa";
            this.txtPesquisa.Size = new System.Drawing.Size(261, 20);
            this.txtPesquisa.TabIndex = 32;
            // 
            // btnNovo
            // 
            this.btnNovo.Location = new System.Drawing.Point(23, 378);
            this.btnNovo.Name = "btnNovo";
            this.btnNovo.Size = new System.Drawing.Size(75, 23);
            this.btnNovo.TabIndex = 26;
            this.btnNovo.Text = "Novo";
            this.btnNovo.Click += new System.EventHandler(this.btnNovo_Click);
            // 
            // groupDados
            // 
            this.groupDados.Controls.Add(this.txtRamo);
            this.groupDados.Controls.Add(this.txtEndereco);
            this.groupDados.Controls.Add(this.txtCnpj);
            this.groupDados.Controls.Add(this.txtFantasia);
            this.groupDados.Controls.Add(this.txtNome);
            this.groupDados.Controls.Add(this.txtCod);
            this.groupDados.Controls.Add(this.lblRamo);
            this.groupDados.Controls.Add(this.lblEndereco);
            this.groupDados.Controls.Add(this.lblCnpj);
            this.groupDados.Controls.Add(this.lblFantasia);
            this.groupDados.Controls.Add(this.lblNome);
            this.groupDados.Controls.Add(this.lblCodigo);
            this.groupDados.Location = new System.Drawing.Point(10, 12);
            this.groupDados.Name = "groupDados";
            this.groupDados.Size = new System.Drawing.Size(444, 269);
            this.groupDados.TabIndex = 23;
            this.groupDados.Text = "Dados:";
            // 
            // txtRamo
            // 
            this.txtRamo.EditValue = "";
            this.txtRamo.Location = new System.Drawing.Point(111, 231);
            this.txtRamo.Name = "txtRamo";
            this.txtRamo.Size = new System.Drawing.Size(218, 20);
            this.txtRamo.TabIndex = 11;
            // 
            // FrmForncedores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 411);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.btnPesquisar);
            this.Controls.Add(this.btnExcluir);
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.lblPesquisa);
            this.Controls.Add(this.groupControlInfo);
            this.Controls.Add(this.txtPesquisa);
            this.Controls.Add(this.btnNovo);
            this.Controls.Add(this.gridControlFornecedor);
            this.Controls.Add(this.groupDados);
            this.KeyPreview = true;
            this.Name = "FrmForncedores";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro e Manutenção de Fornecedores";
            this.Load += new System.EventHandler(this.FrmForncedores_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtEndereco.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCnpj.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFantasia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNome.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlInfo)).EndInit();
            this.groupControlInfo.ResumeLayout(false);
            this.groupControlInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewFornecedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlFornecedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPesquisa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupDados)).EndInit();
            this.groupDados.ResumeLayout(false);
            this.groupDados.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRamo.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnSalvar;
        private DevExpress.XtraEditors.TextEdit txtEndereco;
        private DevExpress.XtraEditors.TextEdit txtCnpj;
        private DevExpress.XtraEditors.TextEdit txtFantasia;
        private DevExpress.XtraEditors.TextEdit txtNome;
        private DevExpress.XtraEditors.TextEdit txtCod;
        private DevExpress.XtraEditors.LabelControl lblRamo;
        private DevExpress.XtraEditors.SimpleButton btnPesquisar;
        private DevExpress.XtraEditors.SimpleButton btnExcluir;
        private DevExpress.XtraEditors.SimpleButton btnEditar;
        private DevExpress.XtraEditors.LabelControl lblEndereco;
        private DevExpress.XtraEditors.LabelControl lblCnpj;
        private DevExpress.XtraEditors.LabelControl lblFantasia;
        private DevExpress.XtraEditors.SimpleButton btnCancelar;
        private DevExpress.XtraEditors.LabelControl lblNome;
        private DevExpress.XtraEditors.LabelControl lblPesquisa;
        private DevExpress.XtraEditors.LabelControl lblCodigo;
        private DevExpress.XtraEditors.LabelControl lblInfo;
        private DevExpress.XtraEditors.GroupControl groupControlInfo;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewFornecedor;
        private DevExpress.XtraGrid.GridControl gridControlFornecedor;
        private DevExpress.XtraEditors.TextEdit txtPesquisa;
        private DevExpress.XtraEditors.SimpleButton btnNovo;
        private DevExpress.XtraEditors.GroupControl groupDados;
        private DevExpress.XtraEditors.TextEdit txtRamo;
    }
}