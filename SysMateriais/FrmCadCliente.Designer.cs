﻿namespace SysMateriais
{
    partial class FrmCadCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupDados = new DevExpress.XtraEditors.GroupControl();
            this.txtData = new DevExpress.XtraEditors.DateEdit();
            this.txtRg = new DevExpress.XtraEditors.TextEdit();
            this.txtCpf = new DevExpress.XtraEditors.TextEdit();
            this.txtEndereco = new DevExpress.XtraEditors.TextEdit();
            this.txtNome = new DevExpress.XtraEditors.TextEdit();
            this.txtCod = new DevExpress.XtraEditors.TextEdit();
            this.lblCompra = new DevExpress.XtraEditors.LabelControl();
            this.lblRg = new DevExpress.XtraEditors.LabelControl();
            this.lblCpf = new DevExpress.XtraEditors.LabelControl();
            this.lblEndereco = new DevExpress.XtraEditors.LabelControl();
            this.lblNome = new DevExpress.XtraEditors.LabelControl();
            this.lblCodigo = new DevExpress.XtraEditors.LabelControl();
            this.gridControlCliente = new DevExpress.XtraGrid.GridControl();
            this.gridViewCliente = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.groupControlInfo = new DevExpress.XtraEditors.GroupControl();
            this.lblInfo = new DevExpress.XtraEditors.LabelControl();
            this.btnNovo = new DevExpress.XtraEditors.SimpleButton();
            this.btnSalvar = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditar = new DevExpress.XtraEditors.SimpleButton();
            this.btnExcluir = new DevExpress.XtraEditors.SimpleButton();
            this.btnPesquisar = new DevExpress.XtraEditors.SimpleButton();
            this.txtPesquisa = new DevExpress.XtraEditors.TextEdit();
            this.lblPesquisa = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.groupDados)).BeginInit();
            this.groupDados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtData.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtData.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRg.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCpf.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndereco.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNome.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlInfo)).BeginInit();
            this.groupControlInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPesquisa.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupDados
            // 
            this.groupDados.Controls.Add(this.txtData);
            this.groupDados.Controls.Add(this.txtRg);
            this.groupDados.Controls.Add(this.txtCpf);
            this.groupDados.Controls.Add(this.txtEndereco);
            this.groupDados.Controls.Add(this.txtNome);
            this.groupDados.Controls.Add(this.txtCod);
            this.groupDados.Controls.Add(this.lblCompra);
            this.groupDados.Controls.Add(this.lblRg);
            this.groupDados.Controls.Add(this.lblCpf);
            this.groupDados.Controls.Add(this.lblEndereco);
            this.groupDados.Controls.Add(this.lblNome);
            this.groupDados.Controls.Add(this.lblCodigo);
            this.groupDados.Location = new System.Drawing.Point(12, 12);
            this.groupDados.Name = "groupDados";
            this.groupDados.Size = new System.Drawing.Size(444, 269);
            this.groupDados.TabIndex = 0;
            this.groupDados.Text = "Dados:";
            // 
            // txtData
            // 
            this.txtData.EditValue = new System.DateTime(2013, 12, 31, 18, 41, 13, 980);
            this.txtData.Location = new System.Drawing.Point(111, 227);
            this.txtData.Name = "txtData";
            this.txtData.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtData.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtData.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.TouchUI;
            this.txtData.Properties.Mask.EditMask = "dd-mm-yyyy";
            this.txtData.Properties.NullDate = new System.DateTime(2013, 12, 31, 18, 45, 6, 989);
            this.txtData.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.False;
            this.txtData.Size = new System.Drawing.Size(100, 20);
            this.txtData.TabIndex = 11;
            // 
            // txtRg
            // 
            this.txtRg.EditValue = "";
            this.txtRg.Location = new System.Drawing.Point(111, 186);
            this.txtRg.Name = "txtRg";
            this.txtRg.Size = new System.Drawing.Size(100, 20);
            this.txtRg.TabIndex = 10;
            // 
            // txtCpf
            // 
            this.txtCpf.Location = new System.Drawing.Point(111, 140);
            this.txtCpf.Name = "txtCpf";
            this.txtCpf.Size = new System.Drawing.Size(218, 20);
            this.txtCpf.TabIndex = 9;
            // 
            // txtEndereco
            // 
            this.txtEndereco.Location = new System.Drawing.Point(111, 98);
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(218, 20);
            this.txtEndereco.TabIndex = 8;
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(111, 58);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(328, 20);
            this.txtNome.TabIndex = 7;
            // 
            // txtCod
            // 
            this.txtCod.Location = new System.Drawing.Point(111, 24);
            this.txtCod.Name = "txtCod";
            this.txtCod.Size = new System.Drawing.Size(65, 20);
            this.txtCod.TabIndex = 1;
            // 
            // lblCompra
            // 
            this.lblCompra.Location = new System.Drawing.Point(5, 234);
            this.lblCompra.Name = "lblCompra";
            this.lblCompra.Size = new System.Drawing.Size(100, 13);
            this.lblCompra.TabIndex = 6;
            this.lblCompra.Text = "Data de Nascimento:";
            // 
            // lblRg
            // 
            this.lblRg.Location = new System.Drawing.Point(5, 192);
            this.lblRg.Name = "lblRg";
            this.lblRg.Size = new System.Drawing.Size(18, 13);
            this.lblRg.TabIndex = 5;
            this.lblRg.Text = "RG:";
            // 
            // lblCpf
            // 
            this.lblCpf.Location = new System.Drawing.Point(5, 150);
            this.lblCpf.Name = "lblCpf";
            this.lblCpf.Size = new System.Drawing.Size(23, 13);
            this.lblCpf.TabIndex = 4;
            this.lblCpf.Text = "CPF:";
            // 
            // lblEndereco
            // 
            this.lblEndereco.Location = new System.Drawing.Point(5, 108);
            this.lblEndereco.Name = "lblEndereco";
            this.lblEndereco.Size = new System.Drawing.Size(49, 13);
            this.lblEndereco.TabIndex = 3;
            this.lblEndereco.Text = "Endereço:";
            // 
            // lblNome
            // 
            this.lblNome.Location = new System.Drawing.Point(5, 66);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(31, 13);
            this.lblNome.TabIndex = 2;
            this.lblNome.Text = "Nome:";
            // 
            // lblCodigo
            // 
            this.lblCodigo.Location = new System.Drawing.Point(5, 24);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(37, 13);
            this.lblCodigo.TabIndex = 1;
            this.lblCodigo.Text = "Código:";
            // 
            // gridControlCliente
            // 
            this.gridControlCliente.Location = new System.Drawing.Point(462, 47);
            this.gridControlCliente.MainView = this.gridViewCliente;
            this.gridControlCliente.Name = "gridControlCliente";
            this.gridControlCliente.Size = new System.Drawing.Size(465, 234);
            this.gridControlCliente.TabIndex = 1;
            this.gridControlCliente.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewCliente});
            // 
            // gridViewCliente
            // 
            this.gridViewCliente.GridControl = this.gridControlCliente;
            this.gridViewCliente.Name = "gridViewCliente";
            this.gridViewCliente.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridViewCliente_RowClick);
            // 
            // groupControlInfo
            // 
            this.groupControlInfo.Controls.Add(this.lblInfo);
            this.groupControlInfo.Location = new System.Drawing.Point(462, 294);
            this.groupControlInfo.Name = "groupControlInfo";
            this.groupControlInfo.Size = new System.Drawing.Size(465, 107);
            this.groupControlInfo.TabIndex = 14;
            this.groupControlInfo.Text = "Informações:";
            // 
            // lblInfo
            // 
            this.lblInfo.Location = new System.Drawing.Point(5, 37);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(0, 13);
            this.lblInfo.TabIndex = 0;
            // 
            // btnNovo
            // 
            this.btnNovo.Location = new System.Drawing.Point(25, 378);
            this.btnNovo.Name = "btnNovo";
            this.btnNovo.Size = new System.Drawing.Size(75, 23);
            this.btnNovo.TabIndex = 15;
            this.btnNovo.Text = "Novo";
            this.btnNovo.Click += new System.EventHandler(this.btnNovo_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(106, 378);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(75, 23);
            this.btnSalvar.TabIndex = 16;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(187, 378);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 17;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.Location = new System.Drawing.Point(268, 378);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(75, 23);
            this.btnEditar.TabIndex = 18;
            this.btnEditar.Text = "Editar";
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnExcluir
            // 
            this.btnExcluir.Location = new System.Drawing.Point(349, 378);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnExcluir.TabIndex = 19;
            this.btnExcluir.Text = "Excluir";
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.Location = new System.Drawing.Point(852, 9);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(75, 23);
            this.btnPesquisar.TabIndex = 22;
            this.btnPesquisar.Text = "Pesquisar";
            this.btnPesquisar.Click += new System.EventHandler(this.btnPesquisar_Click);
            // 
            // txtPesquisa
            // 
            this.txtPesquisa.Location = new System.Drawing.Point(563, 12);
            this.txtPesquisa.Name = "txtPesquisa";
            this.txtPesquisa.Size = new System.Drawing.Size(261, 20);
            this.txtPesquisa.TabIndex = 21;
            // 
            // lblPesquisa
            // 
            this.lblPesquisa.Location = new System.Drawing.Point(462, 15);
            this.lblPesquisa.Name = "lblPesquisa";
            this.lblPesquisa.Size = new System.Drawing.Size(95, 13);
            this.lblPesquisa.TabIndex = 20;
            this.lblPesquisa.Text = "Pesquisa por Nome:";
            // 
            // FrmCadCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 411);
            this.Controls.Add(this.btnPesquisar);
            this.Controls.Add(this.txtPesquisa);
            this.Controls.Add(this.lblPesquisa);
            this.Controls.Add(this.btnExcluir);
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.btnNovo);
            this.Controls.Add(this.groupControlInfo);
            this.Controls.Add(this.gridControlCliente);
            this.Controls.Add(this.groupDados);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCadCliente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro e Manutenção de Clientes";
            this.Load += new System.EventHandler(this.FrmCadCliente_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupDados)).EndInit();
            this.groupDados.ResumeLayout(false);
            this.groupDados.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtData.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtData.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRg.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCpf.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndereco.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNome.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlInfo)).EndInit();
            this.groupControlInfo.ResumeLayout(false);
            this.groupControlInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPesquisa.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupDados;
        private DevExpress.XtraEditors.LabelControl lblCompra;
        private DevExpress.XtraEditors.LabelControl lblRg;
        private DevExpress.XtraEditors.LabelControl lblCpf;
        private DevExpress.XtraEditors.LabelControl lblEndereco;
        private DevExpress.XtraEditors.LabelControl lblNome;
        private DevExpress.XtraEditors.LabelControl lblCodigo;
        private DevExpress.XtraEditors.TextEdit txtRg;
        private DevExpress.XtraEditors.TextEdit txtCpf;
        private DevExpress.XtraEditors.TextEdit txtEndereco;
        private DevExpress.XtraEditors.TextEdit txtNome;
        private DevExpress.XtraEditors.TextEdit txtCod;
        private DevExpress.XtraGrid.GridControl gridControlCliente;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewCliente;
        private DevExpress.XtraEditors.GroupControl groupControlInfo;
        private DevExpress.XtraEditors.SimpleButton btnNovo;
        private DevExpress.XtraEditors.SimpleButton btnSalvar;
        private DevExpress.XtraEditors.SimpleButton btnCancelar;
        private DevExpress.XtraEditors.SimpleButton btnEditar;
        private DevExpress.XtraEditors.SimpleButton btnExcluir;
        private DevExpress.XtraEditors.SimpleButton btnPesquisar;
        private DevExpress.XtraEditors.TextEdit txtPesquisa;
        private DevExpress.XtraEditors.LabelControl lblPesquisa;
        private DevExpress.XtraEditors.LabelControl lblInfo;
        private DevExpress.XtraEditors.DateEdit txtData;
    }
}