﻿namespace SysMateriais
{
    partial class FrmConsultaProd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblInformacao = new System.Windows.Forms.Label();
            this.btnPesquisar = new DevExpress.XtraEditors.SimpleButton();
            this.lblPesquisa = new DevExpress.XtraEditors.LabelControl();
            this.txtPesquisa = new DevExpress.XtraEditors.TextEdit();
            this.gridProd = new DevExpress.XtraGrid.GridControl();
            this.viewProd = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.txtPesquisa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridProd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewProd)).BeginInit();
            this.SuspendLayout();
            // 
            // lblInformacao
            // 
            this.lblInformacao.AutoSize = true;
            this.lblInformacao.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblInformacao.Location = new System.Drawing.Point(74, 315);
            this.lblInformacao.Name = "lblInformacao";
            this.lblInformacao.Size = new System.Drawing.Size(332, 13);
            this.lblInformacao.TabIndex = 47;
            this.lblInformacao.Text = "Para selecionar um produto clique duas vezes no produto desejado.";
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.Location = new System.Drawing.Point(6, 271);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(75, 23);
            this.btnPesquisar.TabIndex = 46;
            this.btnPesquisar.Text = "Pesquisar";
            this.btnPesquisar.Click += new System.EventHandler(this.btnPesquisar_Click);
            // 
            // lblPesquisa
            // 
            this.lblPesquisa.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPesquisa.Location = new System.Drawing.Point(6, 236);
            this.lblPesquisa.Name = "lblPesquisa";
            this.lblPesquisa.Size = new System.Drawing.Size(144, 16);
            this.lblPesquisa.TabIndex = 44;
            this.lblPesquisa.Text = "Pesquisa por Nome:";
            // 
            // txtPesquisa
            // 
            this.txtPesquisa.Location = new System.Drawing.Point(156, 235);
            this.txtPesquisa.Name = "txtPesquisa";
            this.txtPesquisa.Size = new System.Drawing.Size(293, 20);
            this.txtPesquisa.TabIndex = 45;
            // 
            // gridProd
            // 
            this.gridProd.Dock = System.Windows.Forms.DockStyle.Top;
            this.gridProd.Location = new System.Drawing.Point(0, 0);
            this.gridProd.MainView = this.viewProd;
            this.gridProd.Name = "gridProd";
            this.gridProd.Size = new System.Drawing.Size(554, 200);
            this.gridProd.TabIndex = 48;
            this.gridProd.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.viewProd});
            // 
            // viewProd
            // 
            this.viewProd.GridControl = this.gridProd;
            this.viewProd.Name = "viewProd";
            this.viewProd.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.viewProd_RowClick);
            // 
            // FrmConsultaProd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(554, 340);
            this.Controls.Add(this.gridProd);
            this.Controls.Add(this.lblInformacao);
            this.Controls.Add(this.btnPesquisar);
            this.Controls.Add(this.lblPesquisa);
            this.Controls.Add(this.txtPesquisa);
            this.Name = "FrmConsultaProd";
            this.Text = "Consulta dos Produtos";
            this.Load += new System.EventHandler(this.FrmConsultaProd_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtPesquisa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridProd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewProd)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblInformacao;
        private DevExpress.XtraEditors.SimpleButton btnPesquisar;
        private DevExpress.XtraEditors.LabelControl lblPesquisa;
        private DevExpress.XtraEditors.TextEdit txtPesquisa;
        private DevExpress.XtraGrid.GridControl gridProd;
        private DevExpress.XtraGrid.Views.Grid.GridView viewProd;
    }
}