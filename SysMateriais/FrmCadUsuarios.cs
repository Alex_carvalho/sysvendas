﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Xpo;
using DevExpress.XtraEditors;
using SysMateriais.DI.Entidades;

namespace SysMateriais
{
    public partial class FrmCadUsuarios : DevExpress.XtraEditors.XtraForm
    {
        public FrmCadUsuarios()
        {
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            if ((txtUsuario.Text == "") || (txtSenha.Text == ""))
            {
                MessageBox.Show("É necessário Digiar o login e a senha para cadastrar.");
            }
            else
            {
                var cadastra = new Usuarios();
                bool retorno;
                retorno = cadastra.CadastraUsuario(txtUsuario.Text, txtSenha.Text);
                if (retorno == true)
                {
                    MessageBox.Show("Usuário cadastrado com sucesso.");
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Ocorreu um erro ao cadastrar.");
                }
            }
        }
    }
}