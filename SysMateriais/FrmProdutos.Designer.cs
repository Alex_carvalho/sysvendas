﻿namespace SysMateriais
{
    partial class FrmProdutos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSalvar = new DevExpress.XtraEditors.SimpleButton();
            this.txtCompra = new DevExpress.XtraEditors.TextEdit();
            this.txtEstoque = new DevExpress.XtraEditors.TextEdit();
            this.txtFornecedor = new DevExpress.XtraEditors.TextEdit();
            this.txtNome = new DevExpress.XtraEditors.TextEdit();
            this.txtCod = new DevExpress.XtraEditors.TextEdit();
            this.lblData = new DevExpress.XtraEditors.LabelControl();
            this.btnPesquisar = new DevExpress.XtraEditors.SimpleButton();
            this.btnExcluir = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditar = new DevExpress.XtraEditors.SimpleButton();
            this.lblEndereco = new DevExpress.XtraEditors.LabelControl();
            this.lblRg = new DevExpress.XtraEditors.LabelControl();
            this.lblFornecedor = new DevExpress.XtraEditors.LabelControl();
            this.btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.lblNome = new DevExpress.XtraEditors.LabelControl();
            this.lblPesquisa = new DevExpress.XtraEditors.LabelControl();
            this.lblCodigo = new DevExpress.XtraEditors.LabelControl();
            this.lblInfo = new DevExpress.XtraEditors.LabelControl();
            this.groupControlInfo = new DevExpress.XtraEditors.GroupControl();
            this.gridViewProd = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridControlProd = new DevExpress.XtraGrid.GridControl();
            this.txtPesquisa = new DevExpress.XtraEditors.TextEdit();
            this.btnNovo = new DevExpress.XtraEditors.SimpleButton();
            this.groupDados = new DevExpress.XtraEditors.GroupControl();
            this.btnProcurar = new DevExpress.XtraEditors.SimpleButton();
            this.txtVenda = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompra.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstoque.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFornecedor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNome.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlInfo)).BeginInit();
            this.groupControlInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewProd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlProd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPesquisa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupDados)).BeginInit();
            this.groupDados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtVenda.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(104, 378);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(75, 23);
            this.btnSalvar.TabIndex = 27;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // txtCompra
            // 
            this.txtCompra.EditValue = "";
            this.txtCompra.Location = new System.Drawing.Point(111, 186);
            this.txtCompra.Name = "txtCompra";
            this.txtCompra.Size = new System.Drawing.Size(100, 20);
            this.txtCompra.TabIndex = 10;
            // 
            // txtEstoque
            // 
            this.txtEstoque.Location = new System.Drawing.Point(111, 140);
            this.txtEstoque.Name = "txtEstoque";
            this.txtEstoque.Size = new System.Drawing.Size(100, 20);
            this.txtEstoque.TabIndex = 9;
            // 
            // txtFornecedor
            // 
            this.txtFornecedor.Location = new System.Drawing.Point(111, 98);
            this.txtFornecedor.Name = "txtFornecedor";
            this.txtFornecedor.Size = new System.Drawing.Size(218, 20);
            this.txtFornecedor.TabIndex = 8;
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(111, 58);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(328, 20);
            this.txtNome.TabIndex = 7;
            // 
            // txtCod
            // 
            this.txtCod.Location = new System.Drawing.Point(111, 24);
            this.txtCod.Name = "txtCod";
            this.txtCod.Size = new System.Drawing.Size(65, 20);
            this.txtCod.TabIndex = 1;
            // 
            // lblData
            // 
            this.lblData.Location = new System.Drawing.Point(5, 234);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(61, 13);
            this.lblData.TabIndex = 6;
            this.lblData.Text = "Valor Venda:";
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.Location = new System.Drawing.Point(850, 9);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(75, 23);
            this.btnPesquisar.TabIndex = 33;
            this.btnPesquisar.Text = "Pesquisar";
            this.btnPesquisar.Click += new System.EventHandler(this.btnPesquisar_Click);
            // 
            // btnExcluir
            // 
            this.btnExcluir.Location = new System.Drawing.Point(347, 378);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnExcluir.TabIndex = 30;
            this.btnExcluir.Text = "Excluir";
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.Location = new System.Drawing.Point(266, 378);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(75, 23);
            this.btnEditar.TabIndex = 29;
            this.btnEditar.Text = "Editar";
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // lblEndereco
            // 
            this.lblEndereco.Location = new System.Drawing.Point(5, 192);
            this.lblEndereco.Name = "lblEndereco";
            this.lblEndereco.Size = new System.Drawing.Size(68, 13);
            this.lblEndereco.TabIndex = 5;
            this.lblEndereco.Text = "Valor Compra:";
            // 
            // lblRg
            // 
            this.lblRg.Location = new System.Drawing.Point(5, 150);
            this.lblRg.Name = "lblRg";
            this.lblRg.Size = new System.Drawing.Size(71, 13);
            this.lblRg.TabIndex = 4;
            this.lblRg.Text = "Qtde/Estoque:";
            // 
            // lblFornecedor
            // 
            this.lblFornecedor.Location = new System.Drawing.Point(5, 102);
            this.lblFornecedor.Name = "lblFornecedor";
            this.lblFornecedor.Size = new System.Drawing.Size(59, 13);
            this.lblFornecedor.TabIndex = 3;
            this.lblFornecedor.Text = "Fornecedor:";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(185, 378);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 28;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // lblNome
            // 
            this.lblNome.Location = new System.Drawing.Point(5, 66);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(31, 13);
            this.lblNome.TabIndex = 2;
            this.lblNome.Text = "Nome:";
            // 
            // lblPesquisa
            // 
            this.lblPesquisa.Location = new System.Drawing.Point(460, 15);
            this.lblPesquisa.Name = "lblPesquisa";
            this.lblPesquisa.Size = new System.Drawing.Size(95, 13);
            this.lblPesquisa.TabIndex = 31;
            this.lblPesquisa.Text = "Pesquisa por Nome:";
            // 
            // lblCodigo
            // 
            this.lblCodigo.Location = new System.Drawing.Point(5, 24);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(37, 13);
            this.lblCodigo.TabIndex = 1;
            this.lblCodigo.Text = "Código:";
            // 
            // lblInfo
            // 
            this.lblInfo.Location = new System.Drawing.Point(5, 37);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(0, 13);
            this.lblInfo.TabIndex = 0;
            // 
            // groupControlInfo
            // 
            this.groupControlInfo.Controls.Add(this.lblInfo);
            this.groupControlInfo.Location = new System.Drawing.Point(460, 289);
            this.groupControlInfo.Name = "groupControlInfo";
            this.groupControlInfo.Size = new System.Drawing.Size(465, 112);
            this.groupControlInfo.TabIndex = 25;
            this.groupControlInfo.Text = "Informações:";
            // 
            // gridViewProd
            // 
            this.gridViewProd.GridControl = this.gridControlProd;
            this.gridViewProd.Name = "gridViewProd";
            this.gridViewProd.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridViewProd_RowClick);
            // 
            // gridControlProd
            // 
            this.gridControlProd.Location = new System.Drawing.Point(460, 47);
            this.gridControlProd.MainView = this.gridViewProd;
            this.gridControlProd.Name = "gridControlProd";
            this.gridControlProd.Size = new System.Drawing.Size(465, 234);
            this.gridControlProd.TabIndex = 24;
            this.gridControlProd.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewProd});
            // 
            // txtPesquisa
            // 
            this.txtPesquisa.Location = new System.Drawing.Point(561, 12);
            this.txtPesquisa.Name = "txtPesquisa";
            this.txtPesquisa.Size = new System.Drawing.Size(261, 20);
            this.txtPesquisa.TabIndex = 32;
            // 
            // btnNovo
            // 
            this.btnNovo.Location = new System.Drawing.Point(23, 378);
            this.btnNovo.Name = "btnNovo";
            this.btnNovo.Size = new System.Drawing.Size(75, 23);
            this.btnNovo.TabIndex = 26;
            this.btnNovo.Text = "Novo";
            this.btnNovo.Click += new System.EventHandler(this.btnNovo_Click);
            // 
            // groupDados
            // 
            this.groupDados.Controls.Add(this.btnProcurar);
            this.groupDados.Controls.Add(this.txtVenda);
            this.groupDados.Controls.Add(this.txtCompra);
            this.groupDados.Controls.Add(this.txtEstoque);
            this.groupDados.Controls.Add(this.txtFornecedor);
            this.groupDados.Controls.Add(this.txtNome);
            this.groupDados.Controls.Add(this.txtCod);
            this.groupDados.Controls.Add(this.lblData);
            this.groupDados.Controls.Add(this.lblEndereco);
            this.groupDados.Controls.Add(this.lblRg);
            this.groupDados.Controls.Add(this.lblFornecedor);
            this.groupDados.Controls.Add(this.lblNome);
            this.groupDados.Controls.Add(this.lblCodigo);
            this.groupDados.Location = new System.Drawing.Point(10, 12);
            this.groupDados.Name = "groupDados";
            this.groupDados.Size = new System.Drawing.Size(444, 269);
            this.groupDados.TabIndex = 23;
            this.groupDados.Text = "Dados:";
            // 
            // btnProcurar
            // 
            this.btnProcurar.Location = new System.Drawing.Point(337, 97);
            this.btnProcurar.Name = "btnProcurar";
            this.btnProcurar.Size = new System.Drawing.Size(75, 23);
            this.btnProcurar.TabIndex = 34;
            this.btnProcurar.Text = "Procurar";
            this.btnProcurar.Click += new System.EventHandler(this.btnProcurar_Click);
            // 
            // txtVenda
            // 
            this.txtVenda.EditValue = "";
            this.txtVenda.Location = new System.Drawing.Point(111, 227);
            this.txtVenda.Name = "txtVenda";
            this.txtVenda.Size = new System.Drawing.Size(100, 20);
            this.txtVenda.TabIndex = 11;
            // 
            // FrmProdutos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 411);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.btnPesquisar);
            this.Controls.Add(this.btnExcluir);
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.lblPesquisa);
            this.Controls.Add(this.groupControlInfo);
            this.Controls.Add(this.txtPesquisa);
            this.Controls.Add(this.btnNovo);
            this.Controls.Add(this.gridControlProd);
            this.Controls.Add(this.groupDados);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmProdutos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadasto e Manutenção de Produtos";
            this.Load += new System.EventHandler(this.FrmProdutos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtCompra.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEstoque.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFornecedor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNome.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlInfo)).EndInit();
            this.groupControlInfo.ResumeLayout(false);
            this.groupControlInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewProd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlProd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPesquisa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupDados)).EndInit();
            this.groupDados.ResumeLayout(false);
            this.groupDados.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtVenda.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnSalvar;
        private DevExpress.XtraEditors.TextEdit txtCompra;
        private DevExpress.XtraEditors.TextEdit txtEstoque;
        public DevExpress.XtraEditors.TextEdit txtFornecedor;
        private DevExpress.XtraEditors.TextEdit txtNome;
        private DevExpress.XtraEditors.TextEdit txtCod;
        private DevExpress.XtraEditors.LabelControl lblData;
        private DevExpress.XtraEditors.SimpleButton btnPesquisar;
        private DevExpress.XtraEditors.SimpleButton btnExcluir;
        private DevExpress.XtraEditors.SimpleButton btnEditar;
        private DevExpress.XtraEditors.LabelControl lblEndereco;
        private DevExpress.XtraEditors.LabelControl lblRg;
        private DevExpress.XtraEditors.LabelControl lblFornecedor;
        private DevExpress.XtraEditors.SimpleButton btnCancelar;
        private DevExpress.XtraEditors.LabelControl lblNome;
        private DevExpress.XtraEditors.LabelControl lblPesquisa;
        private DevExpress.XtraEditors.LabelControl lblCodigo;
        private DevExpress.XtraEditors.LabelControl lblInfo;
        private DevExpress.XtraEditors.GroupControl groupControlInfo;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewProd;
        private DevExpress.XtraGrid.GridControl gridControlProd;
        private DevExpress.XtraEditors.TextEdit txtPesquisa;
        private DevExpress.XtraEditors.SimpleButton btnNovo;
        private DevExpress.XtraEditors.GroupControl groupDados;
        private DevExpress.XtraEditors.TextEdit txtVenda;
        private DevExpress.XtraEditors.SimpleButton btnProcurar;
    }
}