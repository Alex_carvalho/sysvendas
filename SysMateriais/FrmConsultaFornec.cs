﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Xpo;
using DevExpress.XtraEditors;
using SysMateriais.DI.Entidades;

namespace SysMateriais
{
    public partial class FrmConsultaFornec : DevExpress.XtraEditors.XtraForm
    {
        FrmProdutos _frmProdutos;

        public FrmConsultaFornec(FrmProdutos frmProdutos)
        {
            this._frmProdutos = frmProdutos;
            InitializeComponent();
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            var pesquisa = new Fornecedor();
            DataTable dtRetorno = pesquisa.RetornaPesquisa(txtPesquisa.Text);
            gridForn.DataSource = dtRetorno;
        }

        private void FrmConsultaForm_Load(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void RefreshData()
        {
            var refresh = new Fornecedor();
            gridForn.DataSource = refresh.Data();
            gridForn.Refresh();
            viewForne.BestFitColumns();
        }

        private void viewForne_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            object nome = viewForne.GetRowCellValue(viewForne.FocusedRowHandle, "Nome");
            _frmProdutos.txtFornecedor.Text = nome.ToString();

            _frmProdutos.txtFornecedor.Refresh();
            this.Hide();
        }
    }
}