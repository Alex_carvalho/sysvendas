﻿using System;
using System.Data;
using SysMateriais.DI.Factory;
using DevExpress.Xpo;

namespace SysMateriais.DI.Entidades
{
    public class Clientes
    {
        private int _id;
        private string _nome;
        private string _endereco;
        private DateTime _dataNascimento;
        private string _cpf;
        private string _rg;
        public Clientes()
            :base(){}

        public Clientes(string pNome, string pEndereco, DateTime pDataNascimento, string pCpf, string pRg)
        {
            _nome = pNome;
            _endereco = pEndereco;
            _dataNascimento = pDataNascimento;
            _cpf = pCpf;
            _rg = pRg;
        }

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Nome
        {
            get { return _nome; }
            set { _nome = value; }
        }

        public string Endereco
        {
            get { return _endereco; }
            set { _endereco = value; }
        }
        public DateTime DataNascimento
        {
            get { return _dataNascimento; }
            set { _dataNascimento = value; }
        }

        public string Cpf
        {
            get { return _cpf; }
            set { _cpf = value; }
        }

        public string Rg
        {
            get { return _rg; }
            set { _rg = value; }
        }

        public bool Cadastrar()
        {
            var cadastra = new ClientesFactory(_nome, _endereco, _dataNascimento, _cpf, _rg);
            bool retorno = cadastra.CadastraCliente();
            return retorno;
        }

        public DataTable Data()
        {
            var dt = new DataTable();
            var con = new Conexao();
            con.Conn();
            if (con.Conn())
            {
                var retornadt = new ClientesFactory();
                dt = retornadt.RetornaClientes();
                return dt;
            }
            return dt;
        }

        public bool AlteraCli(int pId)
        {
            _id = pId;
            var altera = new ClientesFactory(_nome, _endereco, _dataNascimento, _cpf, _rg);
            bool retorno = altera.AlteraCliente(_id);
            return retorno;
        }

        public bool DeletaCli(int pId)
        {
            _id = pId;
            var deleta = new ClientesFactory();
            bool retorno = deleta.ExcluiCliente(_id);
            return retorno;
        }

        public DataTable RetornaPesquisa(string pParam)
        {
            var pesquisa = new ClientesFactory();
            string parametro = pParam;
            DataTable dtPes = pesquisa.Pesquisa(parametro);
            return dtPes;
        }
    }
}
