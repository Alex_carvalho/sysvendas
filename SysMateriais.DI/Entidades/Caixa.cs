﻿using System.Data;
using SysMateriais.DI.Factory;

namespace SysMateriais.DI.Entidades
{
    public class Caixa
    {
        private int _clienteId;
        private int _produtoId;

        public Caixa()
        {
        }

        public Caixa(int clienteId, int produtoId)
        {
            _clienteId = clienteId;
            _produtoId = produtoId;
        }

        public DataTable RetornaVendas()
        {
            var retorno = new CaixaFactory();

            DataTable dt = retorno.RetornaVendas();
            return dt;
        }

        public int RetornaProximoId()
        {
            var caixa = new CaixaFactory();
            int codigo = caixa.RetornaProximoId();
            return codigo;
        }

        public bool FechaVenda(int pIdVenda, string pValorTotal)
        {
            int idVenda = pIdVenda;
            string valorTotal = pValorTotal;

            var caixa = new CaixaFactory();
            bool retorno = caixa.FechaVenda(idVenda, valorTotal);
            return retorno;
        }

        public bool RegistraVenda(int pClienteId, int pProdutoId, int pIdVenda, double pValorParcial)
        {
            int idVenda = pIdVenda;
            double valorParcial = pValorParcial;
            _clienteId = pClienteId;
            _produtoId = pProdutoId;

            var registra = new CaixaFactory(_clienteId, _produtoId);
            bool retorno = registra.RegistraVenda(idVenda, valorParcial);
            return retorno;
        }

        public void AtualizaEstoque(int pId, int pEstoque)
        {
            int cod = pId;
            int est = pEstoque;
            var atualiza = new CaixaFactory();
            atualiza.AtualizaEstoque(cod, est);
        }

        public DataTable RetornaDadosVenda(int pCodigo, int pIdVenda)
        {
            int idVenda = pIdVenda;
            int codigo = pCodigo;
            var retornadados = new CaixaFactory();
            DataTable dt = retornadados.RetornaDadosVenda(idVenda, codigo);
            return dt;
        }
    }
}
