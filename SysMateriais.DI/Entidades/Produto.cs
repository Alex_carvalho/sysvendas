﻿using System.Data;
using SysMateriais.DI.Factory;

namespace SysMateriais.DI.Entidades
{
    public class Produto 
    {
        private int _id;
        private string _nome;
        private string _fornecedor;
        private string _estoque;
        private string _valorCompra;
        private string _valorVenda;

        public Produto(){}

        public Produto(string pNome, string pFornecedor, string pEstoque, string pValorCompra, string pValorVenda)
        {
            _nome = pNome;
            _fornecedor = pFornecedor;
            _estoque = pEstoque;
            _valorCompra = pValorCompra;
            _valorVenda = pValorVenda;
        }

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Nome
        {
            get { return _nome; }
            set { _nome = value; }
        }

        public string Fornecedor 
        {
            get { return _fornecedor; }
            set { _fornecedor = value; }
        }

        public string Estoque
        {
            get { return _estoque; }
            set { _estoque = value; }
        }

        public string ValorCompra
        {
            get { return _valorCompra; }
            set { _valorCompra = value; }
        }

        public string ValorVenda
        {
            get { return _valorVenda; }
            set { _valorVenda = value; }
        }

        public bool Cadastrar()
        {
            var cadastra = new ProdutoFactory(_nome, _fornecedor, _estoque, _valorCompra,
                _valorVenda);
            bool retorno = cadastra.CadastraProduto();
            return retorno;
        }

        public DataTable Data()
        {
            var dt = new DataTable();
            var con = new Conexao();
            con.Conn();
            if (con.Conn())
            {
                var retornadt = new ProdutoFactory();
                dt = retornadt.RetornaProduto();
                return dt;
            }
            return dt;
        }

        public bool AlteraProduto(int pId)
        {
            _id = pId;
            var altera = new ProdutoFactory(_nome, _fornecedor, _estoque, _valorCompra, _valorVenda);
            bool retorno = altera.AlteraProduto(_id);
            return retorno;
        }

        public bool DeletaProduto(int pId)
        {
            _id = pId;
            var deleta = new ProdutoFactory();
            bool retorno = deleta.ExcluiProduto(_id);
            return retorno;
        }

        public DataTable RetornaPesquisa(string pParam)
        {
            var pesquisa = new ProdutoFactory();
            string parametro = pParam;
            DataTable dtPes = pesquisa.Pesquisa(parametro);
            return dtPes;
        }
    }
}
