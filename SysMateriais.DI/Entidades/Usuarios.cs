﻿using DevExpress.Xpo;
using SysMateriais.DI.Factory;

namespace SysMateriais.DI.Entidades
{
    public class Usuarios 
    {
        public Usuarios()
            :base()
        {
        }

        private string _nome;
        public string Nome
        {
            get { return _nome; }
            set { _nome = value; }
        }

        private string _senha;

        public string Senha
        {
            get { return _senha; }
            set { _senha = value; }
        }
        public bool CadastraUsuario(string pNome, string pSenha)
        {
            string nome = pNome;
            string senha = pSenha;

            var cadastra = new UsuariosFactory();
            bool retorno = cadastra.CadastraUsuario(nome, senha);
            return retorno;
        }

        public bool VerificaLogin(string pNome, string pSenha)
        {
            string nome = pNome;
            string senha = pSenha;
            var verifica = new UsuariosFactory();
            bool retorno = verifica.VerificaLogin(nome, senha);
            return retorno;
        }

        public bool RetornaSeExiste()
        {
            var usuario = new UsuariosFactory();
            bool retorno = usuario.RetornaSeExiste();
            return retorno;
        }
    }
}
