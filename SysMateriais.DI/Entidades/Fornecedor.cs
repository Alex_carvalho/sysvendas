﻿using System.Data;
using DevExpress.Xpo;
using SysMateriais.DI.Factory;

namespace SysMateriais.DI.Entidades
{
    public class Fornecedor 
    {
        private int _id;
        private string _nome;
        private string _endereco;
        private string _fantasia;
        private string _cnpj;
        private string _ramo;

        public Fornecedor()
            :base(){} 

        public Fornecedor(string pNome, string pFantasia, string pEndereco, string pCnpj, string pRamo)
        {
            _nome = pNome;
            _fantasia = pFantasia;
            _endereco = pEndereco;
            _cnpj = pCnpj;
            _ramo = pRamo;
        }

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Nome
        {
            get { return _nome; }
            set { _nome = value; }
        }

        public string Endereco
        {
            get { return _endereco; }
            set { _endereco = value; }
        }

        public string Fantasia
        {
            get { return _fantasia; }
            set { _fantasia = value; }
        }
        public string Cnpj
        {
            get { return _cnpj; }
            set { _cnpj = value; }
        }

        public string Ramo
        {
            get { return _ramo; }
            set { _ramo = value; }
        }

        public bool Cadastrar()
        {
            var cadastra = new FornecedorFactory(_nome, _fantasia, _endereco, _cnpj, _ramo);
            bool retorno = cadastra.CadastraFornecedor();
            return retorno;
        }

        public DataTable Data()
        {
            var dt = new DataTable();
            var con = new Conexao();
            con.Conn();
            if (con.Conn())
            {
                var retornadt = new FornecedorFactory();
                dt = retornadt.RetornaFornecedor();
                return dt;
            }
            return dt;
        }

        public bool AlteraFornecedor(int pId)
        {
            _id = pId;
            var altera = new FornecedorFactory(_nome, _fantasia, _endereco, _cnpj, _ramo);
            bool retorno;
            retorno = altera.AlteraFornecedor(_id);
            return retorno;
        }

        public bool DeletaFornecedor(int pId)
        {
            _id = pId;
            var deleta = new FornecedorFactory();
            bool retorno;
            retorno = deleta.ExcluiFornecedor(_id);
            return retorno;
        }

        public DataTable RetornaPesquisa(string pParam)
        {
            var pesquisa = new FornecedorFactory();
            string parametro = pParam;
            DataTable dtPes = pesquisa.Pesquisa(parametro);
            return dtPes;
        }
    }
}
