﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysMateriais.DI.Factory
{
    public class UsuariosFactory
    {
        public UsuariosFactory()
        {
        }

        public bool CadastraUsuario(string pNome, string pSenha)
        {
            string nome = pNome;
            string senha = pSenha;
            var con = new Conexao();
            var cone = new SqlConnection(con.Con());
            try
            {
                string strCadastra = "INSERT INTO Usuarios (Usuario, Senha) VALUES (@nUsuario, @nSenha)";
                var comando = new SqlCommand(strCadastra, cone);
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@nUsuario", nome);
                comando.Parameters.AddWithValue("@nSenha", senha);
                cone.Open();
                comando.ExecuteNonQuery();
                cone.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public bool VerificaLogin(string pNome, string pSenha)
        {
            string nome = pNome;
            string senha = pSenha;
            var con = new Conexao();
            var coneVerifica = new SqlConnection(con.Con());
            try
            {
                string strConsulta = "SELECT * FROM Usuarios WHERE Usuario = @nUsuario AND Senha = @nSenha";
                var comando = new SqlCommand(strConsulta, coneVerifica);
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@nUsuario", nome);
                comando.Parameters.AddWithValue("@nSenha", senha);
                coneVerifica.Open();
                SqlDataReader dr = comando.ExecuteReader();
                if (dr.HasRows)
                {
                    return true;
                }
                else
                {
                    return false;
                }

                coneVerifica.Close();
            }
            catch (Exception)
            {
                return false;
                throw;
            }
            return false;
        }

        public bool RetornaSeExiste()
        {
            var con = new Conexao();
            var conexao = new SqlConnection(con.Con());
            try
            {
                string strVerifica = "SELECT * FROM Usuarios";
                var comando = new SqlCommand(strVerifica, conexao);
                comando.Parameters.Clear();
                conexao.Open();
                SqlDataReader dr = comando.ExecuteReader();
                if (dr.HasRows == false)
                {
                    return false;
                }
                else
                {
                    return true;
                }
                conexao.Close();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
