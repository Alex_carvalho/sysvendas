﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysMateriais.DI.Factory
{
    class FornecedorFactory
    {
        private int _id;
        private readonly string _nome;
        private readonly string _endereco;
        private readonly string _fantasia;
        private readonly string _cnpj;
        private readonly string _ramo;

        public FornecedorFactory()
        {

        }
        
        public FornecedorFactory(string pNome, string pFantasia, string pEndereco, string pCnpj, string pRamo)
        {
            _nome = pNome;
            _endereco = pEndereco;
            _fantasia = pFantasia;
            _cnpj = pCnpj;
            _ramo = pRamo;
        }

        public DataTable RetornaFornecedor()
        {
            var con = new Conexao();
            var rcon = new SqlConnection(con.Con());
            var dt = new DataTable();
            try
            {
                string strFornecedor = "SELECT * FROM Fornecedor";
                var  comando = new SqlCommand(strFornecedor, rcon)
                {
                    CommandType = CommandType.Text
                };
                rcon.Open();
                SqlDataReader dr = comando.ExecuteReader();
                dt.Load(dr);
                return dt;
                rcon.Close();
            }
            catch (Exception)
            {
                return dt;
                throw;
            }
            //return dt;
        }


        public bool CadastraFornecedor()
        {
            var conexaoInsert = new Conexao();
            var conInsere = new SqlConnection(conexaoInsert.Con());
            try
            {
                string comandoInsert = "INSERT INTO Fornecedor (Nome, Fantasia, Endereco, CNPJ, Ramo) " +
                                       "                VALUES (@nNome, @nFantasia, @nEndereco, @nCNPJ, @nRamo)";
                var comando = new SqlCommand(comandoInsert, conInsere);
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@nNome", this._nome);
                comando.Parameters.AddWithValue("@nFantasia", this._fantasia);
                comando.Parameters.AddWithValue("@nEndereco", this._endereco);
                comando.Parameters.AddWithValue("@nCNPJ", this._cnpj);
                comando.Parameters.AddWithValue("@nRamo", this._ramo);
                comando.CommandType = System.Data.CommandType.Text;
                conInsere.Open();
                comando.ExecuteNonQuery();
                return true;
                conInsere.Close();
            }
            catch (Exception)
            {
                return false;
                throw;
            }
            //return false;
        }

        public bool AlteraFornecedor(int pCodigo)
        {
            _id = pCodigo;
            var con = new Conexao();
            var acon = new SqlConnection(con.Con());
            try
            {
                string alteraComando = "UPDATE Fornecedor SET Nome = @nNome, Fantasia = @nFantasia, Endereco = @nEndereco, CNPJ = @nCNPJ, Ramo = @nRamo " +
                                       "WHERE Id=@nId";
                var comando = new SqlCommand(alteraComando, acon);
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@nNome", this._nome);
                comando.Parameters.AddWithValue("@nFantasia", this._fantasia);
                comando.Parameters.AddWithValue("@nEndereco", this._endereco);
                comando.Parameters.AddWithValue("@nCNPJ", this._cnpj);
                comando.Parameters.AddWithValue("@nRamo", this._ramo);
                comando.Parameters.AddWithValue("@nId", _id);
                comando.CommandType = CommandType.Text;
                acon.Open();
                comando.ExecuteNonQuery();
                acon.Close();
                return true;
            }
            catch (SqlException)
            {
            }
            return false;
        }

        public bool ExcluiFornecedor(int pId)
        {
            _id = pId;
            var con = new Conexao();
            var econ = new SqlConnection(con.Con());
            try
            {
                string excluiComando = "DELETE FROM Fornecedor WHERE Id = @n";
                var comando = new SqlCommand(excluiComando, econ);
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@nId", _id);
                comando.CommandType = CommandType.Text;
                econ.Open();
                comando.ExecuteNonQuery();
                econ.Close();
                return true;
            }
            catch (SqlException)
            {
            }
            return false;
        }

        public DataTable Pesquisa(string pParam)
        {
            var con = new Conexao();
            var pes = new SqlConnection(con.Con());
            var dtPes = new DataTable();
            try
            {
                string param = pParam;
                string strPes = "SELECT * FROM Fornecedor WHERE Nome LIKE '%" + param  +"%'";
                var comando = new SqlCommand(strPes, pes);
                comando.Parameters.Clear();
                pes.Open();
                SqlDataReader drPes = comando.ExecuteReader();
                dtPes.Load(drPes);
                return dtPes;
                pes.Close();
            }
            catch (Exception)
            {
                return dtPes;
                throw;
            }
            //return dtPes;
        }
    }
}
