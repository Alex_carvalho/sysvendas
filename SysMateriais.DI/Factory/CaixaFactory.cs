﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace SysMateriais.DI.Factory
{
    public class CaixaFactory
    {
        private readonly int _clienteId;
        private readonly int _produtoId;

        public CaixaFactory()
        {
              
        }

        public CaixaFactory(int clienteId, int produtoId)
        {
            _clienteId = clienteId;
            _produtoId = produtoId;
        }

        public DataTable RetornaVendas()
        {
            var con = new Conexao();
            var dt = new DataTable();
            var conexao = new SqlConnection(con.Con());
            try
            {
                const string strRetornaVendas = "SELECT vendas.Id, clientes.Nome, produtos.Nome, vendas.ValorTotal, vendas.Status " +
                                                "FROM Produtos INNER JOIN Vendas ON vendas.ProdutoId = produtos.Id " +
                                                "INNER JOIN Clientes ON vendas.ClienteId = clientes.Id";
                var comando = new SqlCommand(strRetornaVendas, conexao);
                comando.Parameters.Clear();
                conexao.Open();

                SqlDataReader dr = comando.ExecuteReader();
                dt.Load(dr);
                return dt;
            }
            catch (Exception)
            {
                return dt;
            }
        }

        public int RetornaProximoId()
        {
            var con = new Conexao();
            int proximoId = 0;
            var conexao = new SqlConnection(con.Con());
            try
            {
                const string strRetorna = "SELECT MAX(Id) FROM Vendas";
                var comando = new SqlCommand(strRetorna, conexao);
                conexao.Open();
                SqlDataReader dr = comando.ExecuteReader();
                while (dr.Read())
                {
                    proximoId = Convert.ToInt32(dr[0]);
                    proximoId = proximoId + 1;
                }
                conexao.Close();
                return proximoId;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public bool FechaVenda(int pidVenda, string pValorTotal)
        {
            int idVenda = pidVenda;
            string valorTotal = pValorTotal;
            const string status = "F";

            var con = new Conexao();

            var conexao = new SqlConnection(con.Con());
            try
            {
                const string strFecha = "UPDATE Vendas SET ValorTotal = @nValorTotal, Status = @nStatus WHERE CodigoVenda = @nIdVenda";
                var comando = new SqlCommand(strFecha, conexao);
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@nValorTotal", valorTotal);
                comando.Parameters.AddWithValue("@nStatus", status);
                comando.Parameters.AddWithValue("@nIdVenda", idVenda);
                conexao.Open();
                comando.ExecuteNonQuery();
                conexao.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool RegistraVenda(int pIdVenda, double pValorParcial)
        {
            var con = new Conexao();
            double valorParcial = pValorParcial;
            int codigoVenda = pIdVenda;
            var conexao = new SqlConnection(con.Con());

            try
            {
                const string strRegistraVenda = "INSERT INTO Vendas (ClienteId, ProdutoId, ValorParcial, CodigoVenda) " +
                                                "VALUES (@nClienteId, @nProdutoId, @nValorParcial, @nCodigoVenda)";
                var comando = new SqlCommand(strRegistraVenda, conexao);
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@nClienteId", _clienteId);
                comando.Parameters.AddWithValue("@nProdutoId", _produtoId);
                comando.Parameters.AddWithValue("@nValorParcial", valorParcial);
                comando.Parameters.AddWithValue("@nCodigoVenda", codigoVenda);
                conexao.Open();
                comando.ExecuteNonQuery();
                conexao.Close();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void AtualizaEstoque(int pIdProduto, int pEstoque)
        {
            var con = new Conexao();
            int idProduto = pIdProduto;
            int estoque = pEstoque;
            var conexao = new SqlConnection(con.Con());
            const string strAtualizaEstoque = "UPDATE Produtos SET Estoque = Estoque - @nEstoque WHERE Id = @nId";
            var comando = new SqlCommand(strAtualizaEstoque, conexao);
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@nEstoque", estoque);
            comando.Parameters.AddWithValue("@nId", idProduto);
            conexao.Open();
            comando.ExecuteNonQuery();
            conexao.Close();
        }

        public DataTable RetornaDadosVenda(int pidVenda, int id)
        {
            int idVenda = pidVenda;
            var con = new Conexao();
            var dt = new DataTable();
            var conexao = new SqlConnection(con.Con());
            try
            {
                const string strRetornaVendas = "SELECT Vendas.Id, clientes.Nome, produtos.Nome, vendas.ValorTotal, vendas.Status " +
                                                "FROM Produtos INNER JOIN Vendas ON vendas.ProdutoId = produtos.Id " +
                                                "INNER JOIN Clientes ON Vendas.ClienteId = clientes.Id WHERE Vendas.CodigoVenda = @nIdVenda";
                var comando = new SqlCommand(strRetornaVendas, conexao);
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@nIdVenda", idVenda);
                conexao.Open();

                SqlDataReader dr = comando.ExecuteReader();
                dt.Load(dr);
                return dt;
            }
            catch (Exception)
            {
                return dt;
            }
        }
    }
}
