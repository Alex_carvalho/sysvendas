﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysMateriais.DI.Factory
{
    public class ClientesFactory
    {
        private int _id;
        private readonly string _nome;
        private readonly string _endereco;
        private readonly DateTime _dataNascimento;
        private readonly string _cpf;
        private readonly string _rg;

        public ClientesFactory()
        {

        }
        
        public ClientesFactory(string pNome, string pEndereco, DateTime pDataNascimento, string pCPF, string pRG)
        {
            _nome = pNome;
            _endereco = pEndereco;
            _dataNascimento = pDataNascimento;
            _cpf = pCPF;
            _rg = pRG;
        }

        public DataTable RetornaClientes()
        {
            var con = new Conexao();
            var rcon = new SqlConnection(con.Con());
            var dt = new DataTable();
            try
            {
                string strClientes = "SELECT * FROM Clientes";
                var comando = new SqlCommand(strClientes, rcon);
                comando.CommandType = CommandType.Text;
                rcon.Open();
                SqlDataReader dr = comando.ExecuteReader();
                dt.Load(dr);
                return dt;
                rcon.Close();
            }
            catch (Exception)
            {
                return dt;
                throw;
            }
            //return dt;
        }


        public bool CadastraCliente()
        {
            var conexaoInsert = new Conexao();
            var conInsere = new SqlConnection(conexaoInsert.Con());
            try
            {
                string comandoInsert =
                    "INSERT INTO Clientes (Nome, Endereco, DataNascimento, CPF, RG) " +
                                  "VALUES (@nNome, @nEndereco, @nDataNascimento, @nCPF, @nRG)";
                var comando = new SqlCommand(comandoInsert, conInsere);
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@nNome", this._nome);
                comando.Parameters.AddWithValue("@nEndereco", this._endereco);
                comando.Parameters.AddWithValue("@nDataNascimento", this._dataNascimento);
                comando.Parameters.AddWithValue("@nCPF", this._cpf);
                comando.Parameters.AddWithValue("@nRG", this._rg);
                comando.CommandType = System.Data.CommandType.Text;
                conInsere.Open();
                comando.ExecuteNonQuery();
                return true;
                conInsere.Close();
            }
            catch (Exception)
            {
                return false;
                throw;
            }
            //return false;
        }

        public bool AlteraCliente(int pCodigo)
        {
            _id = pCodigo;
            var con = new Conexao();
            var acon = new SqlConnection(con.Con());
            try
            {
                string alteraComando =
                    "UPDATE Clientes SET Nome = @nNome, Endereco = @nEndereco, DataNascimento = @nDataNascimento, CPF = @nCPF, RG = @nRG " +
                    "WHERE Id=@nId";
                var comando = new SqlCommand(alteraComando, acon);
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@nNome", this._nome);
                comando.Parameters.AddWithValue("@nEndereco", this._endereco);
                comando.Parameters.AddWithValue("@nDataNascimento", this._dataNascimento);
                comando.Parameters.AddWithValue("@nCPF", this._cpf);
                comando.Parameters.AddWithValue("@nRG", this._rg);
                comando.Parameters.AddWithValue("@nId", _id);
                comando.CommandType = CommandType.Text;
                acon.Open();
                comando.ExecuteNonQuery();
                acon.Close();
                return true;
            }
            catch (SqlException ex)
            {
            }
            return false;
        }

        public bool ExcluiCliente(int pId)
        {
            _id = pId;
            var con = new Conexao();
            var econ = new SqlConnection(con.Con());
            try
            {
                string excluiComando = "DELETE FROM Clientes WHERE ClienteId=@nId";
                var comando = new SqlCommand(excluiComando, econ);
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@nCodigo", _id);
                comando.CommandType = CommandType.Text;
                econ.Open();
                comando.ExecuteNonQuery();
                econ.Close();
                return true;
            }
            catch (SqlException ex)
            {
            }
            return false;
        }

        public DataTable Pesquisa(string pParam)
        {
            var con = new Conexao();
            var pes = new SqlConnection(con.Con());
            var dtPes = new DataTable();
            try
            {
                string param = pParam;
                string strPes = "SELECT * FROM Clientes WHERE Nome LIKE '%" + param + "%'";
                var comando = new SqlCommand(strPes, pes);
                comando.Parameters.Clear();
                pes.Open();
                SqlDataReader drPes = comando.ExecuteReader();
                dtPes.Load(drPes);
                return dtPes;
                pes.Close();
            }
            catch (Exception ex)
            {
                return dtPes;
                throw;
            }
            //return dtPes;
        }
    }
}
