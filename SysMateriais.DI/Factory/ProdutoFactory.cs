﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace SysMateriais.DI.Factory
{
    class ProdutoFactory
    {
        private int _id;
        private readonly string _nome;
        private readonly string _fornecedor;
        private readonly string _estoque;
        private readonly string _valorCompra;
        private readonly string _valorVenda;

        public ProdutoFactory()
        {

        }
        
        public ProdutoFactory(string pNome, string pFornecedor, string pEstoque, string pValorCompra, string pValorVenda)
        {
            _nome = pNome;
            _fornecedor = pFornecedor;
            _estoque = pEstoque;
            _valorCompra = pValorCompra;
            _valorVenda = pValorVenda;
        }

        public DataTable RetornaProduto()
        {
            var con = new Conexao();
            var rcon = new SqlConnection(con.Con());
            var dt = new DataTable();
            try
            {
                const string strProduto = "SELECT * FROM Produtos";
                var  comando = new SqlCommand(strProduto, rcon);
                comando.CommandType = CommandType.Text;
                rcon.Open();
                SqlDataReader dr = comando.ExecuteReader();
                dt.Load(dr);
                return dt;
            }
            catch (Exception)
            {
                return dt;
            }
            //return dt;
        }


        public bool CadastraProduto()
        {
            var conexaoInsert = new Conexao();
            var conInsere = new SqlConnection(conexaoInsert.Con());
            try
            {
                const string comandoInsert = "INSERT INTO produtos (Nome, Fornecedor, Estoque, ValorCompra, ValorVenda) " +
                                             "VALUES (@nNome, @nFornecedor, @nEstoque, @nValorCompra, @nValorVenda)";
                var comando = new SqlCommand(comandoInsert, conInsere);
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@nNome", _nome);
                comando.Parameters.AddWithValue("@nFornecedor", _fornecedor);
                comando.Parameters.AddWithValue("@nEstoque", _estoque);
                comando.Parameters.AddWithValue("@nValorCompra", _valorCompra);
                comando.Parameters.AddWithValue("@nValorVenda", _valorVenda);
                comando.CommandType = CommandType.Text;
                conInsere.Open();
                comando.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            //return false;
        }

        public bool AlteraProduto(int pId)
        {
            _id = pId;
            var con = new Conexao();
            var acon = new SqlConnection(con.Con());
            try
            {
                const string alteraComando = "UPDATE Produtos SET Nome = @nNome, Fornecedor = @nFornecedor, Estoque = @nEstoque, ValorCompra = @nValorCompra, ValorVenda = @nValorVenda " +
                                             "WHERE Id = @nId";
                var comando = new SqlCommand(alteraComando, acon);
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@nNome", _nome);
                comando.Parameters.AddWithValue("@nFornecedor", _fornecedor);
                comando.Parameters.AddWithValue("@nEstoque", _estoque);
                comando.Parameters.AddWithValue("@nValorCompra", _valorCompra);
                comando.Parameters.AddWithValue("@nValorVenda", _valorVenda);
                comando.Parameters.AddWithValue("@nId", _id);
                comando.CommandType = CommandType.Text;
                acon.Open();
                comando.ExecuteNonQuery();
                acon.Close();
                return true;
            }
            catch (SqlException)
            {
            }
            return false;
        }

        public bool ExcluiProduto(int pId)
        {
            _id = pId;
            var con = new Conexao();
            var econ = new SqlConnection(con.Con());
            try
            {
                const string excluiComando = "DELETE FROM Produtos WHERE Id = @nId";
                var comando = new SqlCommand(excluiComando, econ);
                comando.Parameters.Clear();
                comando.Parameters.AddWithValue("@nId", _id);
                comando.CommandType = CommandType.Text;
                econ.Open();
                comando.ExecuteNonQuery();
                econ.Close();
                return true;
            }
            catch (SqlException)
            {
            }
            return false;
        }

        public DataTable Pesquisa(string pParam)
        {
            var con = new Conexao();
            var pes = new SqlConnection(con.Con());
            var dtPes = new DataTable();
            try
            {
                string param = pParam;
                string strPes = "SELECT * FROM Produtos WHERE Nome LIKE '%" + param  +"%'";
                var comando = new SqlCommand(strPes, pes);
                comando.Parameters.Clear();
                pes.Open();
                SqlDataReader drPes = comando.ExecuteReader();
                dtPes.Load(drPes);
                return dtPes;
            }
            catch (Exception)
            {
                return dtPes;
            }
            //return dtPes;
        }
    }
}
