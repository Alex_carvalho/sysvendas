USE [SysMateriais]
GO

CREATE TABLE Clientes
(
	[Id] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Nome] [varchar](200) NOT NULL,
	[Endereco] [varchar](200) NOT NULL,
	[DataNascimento] [datetime] NOT NULL,
	[CPF] [varchar](30) NOT NULL,
	[RG] [varchar](30) NOT NULL,
)

CREATE TABLE Vendas
(
	[Id] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[ClienteID] [int] NOT NULL,
	[ProdutoID] [int] NOT NULL,
	[ValorTotal] [varchar](20) NOT NULL DEFAULT ('0'),
	[ValorParcial] [varchar](10) NOT NULL,
	[Status] [char](1) NOT NULL DEFAULT ('A'),
	[CodigoVenda] [int] NOT NULL,
)


CREATE TABLE Usuarios
(
	[Id] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Usuario] [varchar](20) NOT NULL,
	[Senha] [varchar](20) NOT NULL,
)

CREATE TABLE Produtos
(
	[Id] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Nome] [varchar](200) NOT NULL,
	[Fornecedor] [varchar](200) NOT NULL,
	[Estoque] [varchar](10) NOT NULL,
	[ValorCompra] [varchar](10) NOT NULL,
	[ValorVenda] [varchar](10) NOT NULL,
)

CREATE TABLE Fornecedor
(
	[Id] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Nome] [varchar](200) NOT NULL,
	[Fantasia] [varchar](200) NOT NULL,
	[Endereco] [varchar](100) NOT NULL,
	[CNPJ] [varchar](20) NOT NULL,
	[Ramo] [varchar](20) NOT NULL,
)

